## 1.0.0 Release notes (11-11-2022)

### Breaking Changes
- `Rx` target moved to the SYNAncient library
- `UIKit` part of `Keyboard` target moved to the SYNAncient library
- Removed `RxSwift` dependency
- `Connectivity` implementation changed to use only native SDK
- Removed `Alamofire` dependency
- Minimal version raised to iOS 13
- Added `SYN` prefix to all remaining targets

### Enhancements
- Added `firstValueAsync` function for bridging combine and async/await


## 0.0.7 Release notes (07-10-2021)

### Enhancements
- Added Date target with extension for comparing Dates with added values.
- Added Numbers target with extension for formatting number values to Strings.
- Added Rx target with extensions for unwrapping optional Observable values.
- `IdentifiableEnumeration` now contains also `caseIdentifier` which ignores associated values.
- RxSwift raised to 6.x, Alamofire raised to 5.4.1.
- Added URL target with extension for adding query params to URL.
- Added extension for adding body parameters to URLRequest.
- Added `TimeInterval` computed components (minute, hour, day).
- Added extension for comparing Dates with date components in time direction (e.g. if date is less than 10 minutes ago).
- Added extension for clamping number values (setting its limits by range).
- Added getting of date component value.
- Added Combine target with extensions for checking `Completion` result.
- Added keyboard offset handler for SwiftUI.
- Added SwiftUI/Combine loading handling.
- Added SwiftUI/Combine error handling.
- Added is today function to Date extension.
- Added the most common Date formating to Date target.
- Added async compact and flat map.


## 0.0.6 Release notes (21-12-2020)

### Bugfixes
- `KeyboardOffsetHandler` fixed offset calculated incorectly for iPhone 6  plus (14.2) where 'bindEditingBegin' was called before keyboard height update

## 0.0.5 Release notes (09-11-2020)

### Enhancements
- Added `KeyboardOffsetHandler` for moving text fields when keyboard appears.
- Added function for hiding keyboard on tap in view.git 
- Added functions for setting font weight.


## 0.0.4 Release notes (13-10-2020)

### Bugfixes
- Added public `FontPackage` initializer.


## 0.0.3 Release notes (27-08-2020)

### Enhancements
- Added `StringValidator` for validating String length + email format and strong password.
- Added `NestedIdentiableEnumeration` to `Identifiers`. It allows to identify enums super type also.
- Added `IdentifiableError` to `Identifiers`. It allows you to identify error like `IdentifiableEnumeration` .
- Added `GeohashRegion` model for storing globe area and calculating all geohashes included in this area.

### Bugfixes
- Connectivity properties made public.


## 0.0.2 Release notes (28-10-2019)

### Enhancements
- Added `Connectivity` target.
- Added `Routing` target.
- Added `String` and `Font` targets.
- `Package.swift` structure updated.
- Added `Geohash` target.


## 0.0.1 Release notes (28-10-2019)

### Breaking Changes
- Init.

## x.x.x Release notes (dd-mm-yyyy)

### Breaking Changes
### Deprecated
### Enhancements
### Bugfixes
