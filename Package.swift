// swift-tools-version:5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

enum PackageProduct: String, CaseIterable {

    case wholePackage = "SYNUtils"

    var name: String {
        return rawValue
    }

    var product: Product {
        return .library(name: rawValue, targets: targets)
    }

    var targets: [String] {
        switch self {
        case .wholePackage:
            return PackageTarget.allCases.map { $0.name }
        }
    }
}

enum PackageDependency: String, CaseIterable {

    case introspect = "Introspect"

    var dependency: Target.Dependency {
        switch self {
        case .introspect:
            return .product(name: rawValue, package: "SwiftUI-Introspect")
        }
    }

    static var packages: [Package.Dependency] {
        return [
            .package(url: "https://github.com/siteline/SwiftUI-Introspect", from: "0.10.0")
        ]
    }
}

enum PackageTarget: String, CaseIterable {

    case array = "SYNArray"
    case sequence = "SYNSequence"
    case combine = "SYNCombine"
    case connectivity = "SYNConnectivity"
    case date = "SYNDate"
    case font = "SYNFont"
    case geohash = "SYNGeohash"
    case identifiers = "SYNIdentifiers"
    case keyboard = "SYNKeyboard"
    case numbers = "SYNNumbers"
    case oauth2 = "SYNOAuth2"
    case optional = "SYNOptional"
    case string = "SYNString"
    case routing = "SYNRouting"
    case url = "SYNURL"

    var name: String {
        return rawValue
    }

    var dependency: Target.Dependency {
        return Target.Dependency(stringLiteral: rawValue)
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .geohash:
            return [PackageTarget.identifiers.dependency]
        case .identifiers:
            return [PackageTarget.string.dependency]
        case .keyboard:
            return [PackageDependency.introspect.dependency]
        case .string:
            return [PackageTarget.font.dependency]
        default:
            return []
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.target(name: $0.name, dependencies: $0.dependencies) }
    }
}

enum PackageTestTarget: String, CaseIterable {

    case optional = "OptionalTests"
    case array = "ArrayTests"
    case sequence = "SequenceTests"
    case string = "StringTests"
    case identifiers = "IdentifiersTests"
    case geohash = "GeohashTests"
    case oauth2 = "OAuth2Tests"
    case date = "DateTests"
    case numbers = "NumbersTests"
    case url = "URLTests"
    case combine = "CombineTests"

    var name: String {
        return rawValue
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .optional:
            return [PackageTarget.optional.dependency]
        case .array:
            return [PackageTarget.array.dependency]
        case .sequence:
            return [PackageTarget.sequence.dependency]
        case .string:
            return [PackageTarget.string.dependency, PackageTarget.font.dependency]
        case .identifiers:
            return [PackageTarget.identifiers.dependency]
        case .geohash:
            return [PackageTarget.geohash.dependency]
        case .oauth2:
            return [PackageTarget.oauth2.dependency]
        case .date:
            return [PackageTarget.date.dependency]
        case .numbers:
            return [PackageTarget.numbers.dependency]
        case .url:
            return [PackageTarget.url.dependency]
        case .combine:
            return [PackageTarget.combine.dependency]
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.testTarget(name: $0.name, dependencies: $0.dependencies) }
    }
}

let package = Package(
    name: "SYNUtils",
    platforms: [.iOS(.v13), .macOS(.v10_15)],
    // Products define the executables and libraries produced by a package, and make them visible to other packages.
    products: PackageProduct.allCases.map { $0.product },
    // Dependencies declare other packages that this package depends on.
    dependencies: PackageDependency.packages,
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages which this package depends on.
    targets: PackageTarget.targets + PackageTestTarget.targets
)
