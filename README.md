# SYNUtils

## Array
Provides function to get element at any index in array as optional. You then don't need to check its count to avoid `Index out of range` error. Similiar as `.last` or `.first`.



## Combine
Useful extension on components of `Combine` framework.

### Handling loading and errors on `Publisher` chains
You can easily handle loading (changing `isLoading` value) by adding `.handleLoading(in: _, \.isLoading)` to your chain or you can easily handle errors by adding `handleError(with: _)` to your chain. For more info, see [this page in Notion](https://www.notion.so/synetech/General-guidelines-f29b51ae9db74f4fba2e0d48164a793f#a88071d8878f458bb08220bdf6126efd).

### Completion
You can check the completion result with `isSuccess` or `isFailure` variables.

### Async bridge

Extension on `Publisher` that returns the first value in a async/await way. Just call `firstValueAsync` on the `Publisher` instance.



## Connectivity
- `isNetworkAvailable`
- `typeOfCurrentConnection`



## Date

### Other date comparison
Comparation extension with adding different units (e.g. adding 10 minutes or 5 days). It uses `Calendar.Component` as unit.

Example usage:
```swift
let timestamp = getTimestamp() // = 15 minutes in the past
print(Date().isLessThan(10, unit: .minutes, from: timestamp)) // false
print(Date().isLessThan(20, unit: .minutes, from: timestamp)) // true
```

### Date component comparison
You can compare date with date component in given time direction (ago / in future) - e.g. check if the date is less than 10 minutes ago.

Example usage:
```swift
let timestamp = getTimestamp() // = 15 minutes in the past
print(timestamp.isLessThan(10, .minute, .ago)) // false
print(timestamp.isLessThan(20, .minute, .ago)) // true
```

### Time interval components
You can access computed components on `TimeInterval` (provided as static variables) -  `.minute`, `.hour` and `.day`.

### Others
- you can access date component value with `getComponent` func called on `Date` instance



## Font
TODO:  - Describe already existing implementation @tomnvt

### Font weigth
You can set the font weight by calling `withWeigth(_ weigth: UIFont.Weight)` on the font instance. All possible values are listed in documentation of the function.



## Geohash
Provides geohash encoding/decoding and calculating of neighbours.

```swift
Geohash.encode(latitude: 42.6, longitude: -5.6) // "ezs42"
```
```swift
// With specified precision
Geohash.encode(latitude: -25.382708, longitude: -49.265506, 12) // "6gkzwgjzn820"
```
```swift
Geohash.decode("ezs42") // (latitude: 42.60498046875, longitude: -5.60302734375)
```
```swift
Geohash.neighbour("u000", direction: .north) // "u001"
Geohash.neighbour("u000", direction: .south) // "spbp"
Geohash.neighbour("u000", direction: .east) // "u002"
Geohash.neighbour("u000", direction: .west) // "gbpb"
```

Also you can create the `GeohashRegion` model by passing the min/max latitude and longitude (optionally the precision - default is 5) or by passing geohashes of the top left, top right and bottom left corner. Then you can call `getAllGeohashes` on the instance and you will be provided with all geohashes in the given region.

```swift
GeohashRegion(minLatitude: 49.833984375, maxLatitude: 50.009765625, 
              minLongitude: 17.05078125, maxLongitude: 17.40234375, 
              precision: 4).getAllGeohashes() // ["u2uh", "u2u5", "u2uk", "u2u7"]
GeohashRegion(topLeft: "u2uh", topRight: "u2uk", bottomLeft: "u2u5").getAllGeohashes() // ["u2uh", "u2u5", "u2uk", "u2u7"]
```



## Identifiers
Provides possibility to identify enum case even with the name of the enum itself. 
`NestedIdentifiableEnumeration` provides also possibility to identify enum with its super type name as prefix.

If you need to identify error in the format of `typeCase` (e.g. "myErrorAppCrash"), just conform your error to `IdentifiableError` protocol (instead of just `Error`) and then you can reach the identifier in the `errorDescription` property.



## Keyboard

### Keyboard offset handler
Very simiar functionality as the UIKit one, in this case you can just add the `KeyboardAdaptive` modifier to make your views responsive to the keyboard height. The modifier is well documented, so please read it before you use this feature.
In addition, this implemenation has option to specify whether the fields are in static or scroll view. If you use scroll view, it ensures that the whole scroll view is always visible and the scroll offset is changed to make the active field visible just above the keyboard.

### Hiding keyboard
Provides extension on `UIViewController` with which you can set to hide keyboard when tapped in the view. The most common usage is to set the keyboard to hide when user taps in the view of the view controller.



## Numbers
Useful extension and utilities for working with number data types.

### String formatting
- Formatting number to Strings with `roundedString(to: enter_decimal_count_here)`. Implemented for `Double` and `Float` types.

### Clamped to
You can limit the number value to closed range to ensure that the value is changed to the nearest bound if its outside the given range. 



## OAuth2
Provides PKCE pair for verification during OAuth2 process. It generates pair of random String and its hashed and encoded version (`challenge`). You just need to create new instance for each attempt and pass the `challenge` and `method` to the first request. Then to verify the second request, pass the plain `verificationCode` to it.
Everything should be implemented according [this](https://tools.ietf.org/html/rfc7636) specification.



## Optional
Provides possibility to unwrap optional variables with `.unwrap()` function (instead of the two question marks, e.g.  `?? ""`)



## Routing
- openURL
- callPhoneNumber
- startNewMessageInMailApp
- shareURL
- showSafariViewController



## String
- getHtmlAttributedString (font package can be specified if custom fonts are needed in the result)
- capitalizeFirstCharacter
- lowercaseFirstCharacter
- firstLetter
- initials
- hasProtocol (checks if "http://" or "https://" is contained in the string)
- hasSecuredProtocol
- addSecuredProtocol
- strikeThroughText

### String validator
- validates string length (empty, max, min)
- validates email format
- validates strong password (at least three of four character types - uppercased letter, lowercased letter, digit, special case letter)



## URL
Useful extension for modifying `URL` or `URLRequest`.

### Query params
You can add query params to `URL` by calling mutating func `addQueryParams` on its instance.

### Body params
You can add body params to `URLRequest` by calling mutating func `addBodyParams` on its instance.

## Sequence
### Async maps
- `.asyncFlatMap()`, `asyncCompactMap()` allows using maps in asynchronous mode. Works just like `flatMap` or `compactMap`. Parameter `parallel` enables execution in serial or parallel way.
