//
//  Array+ElementAtIndex.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

import Foundation

public extension Array {

    /// Returns optional element at given index (avoids `Index out of range` error)
    /// - Parameter index: index of element you want to return
    func element(at index: Int) -> Element? {
        if index >= 0 && index < count {
            return self[index]
        } else {
            return nil
        }
    }
}
