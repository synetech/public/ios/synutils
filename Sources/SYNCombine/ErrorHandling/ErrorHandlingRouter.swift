//
//  ErrorHandlingRouter.swift
//  
//
//  Created by Lukáš Růžička on 30.04.2021.
//

public protocol ErrorHandlingRouter: AnyObject {
    
    /// Show alert, pop screen or handle the error differently in implementation of this method.
    /// If this class can't handle the error, just throw it to make it be handled by the remaining of the chain.
    /// - Parameter error: Error emited in Combine chain.
    func handleError(_ error: Error) throws

    /// Show alert, pop screen or handle the error differently in implementation of this method.
    /// If user can interact with the handling (e.g. tapping on alert button), you can pass the action in the `handleAction` callback.
    /// If this class can't handle the error, just throw it to make it be handled by the remaining of the chain.
    /// - Parameters:
    ///   - error: Error emited in Combine chain.
    ///   - handleAction: Callback in which you can pass user action from the handling (e.g. tapping on alert button).
    func handleError(_ error: Error, handleAction: @escaping (UserAction) -> Void) throws
}

// MARK: - Default implementation
public extension ErrorHandlingRouter {

    func handleError(_ error: Error) throws {
        try handleError(error, handleAction: { _ in })
    }
}
