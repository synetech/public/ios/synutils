//
//  Publisher+HandleError.swift
//  
//
//  Created by Lukáš Růžička on 30.04.2021.
//

import Combine

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension Publisher {

    /// This chain modifier takes care of handling emitted errors which is handled by the passed `ErrorHandlingRouter`.
    /// The instance can then throw the error back to make it continue through chain.
    /// - Parameter router: Instance of class (probably router) which conforms to `ErrorHandlingRouter`,
    /// - Returns: Chain continuation.
    func handleError(with router: ErrorHandlingRouter) -> AnyPublisher<Self.Output, Error> {
        tryCatch { [weak router] error -> Self in
            try router?.handleError(error)
            throw error
        }
        .eraseToAnyPublisher()
    }

    /// This chain modifier takes care of handling emitted errors which is handled by the passed `ErrorHandlingRouter`.
    /// The instance can then throw the error back to make it continue through chain
    /// or it can call the `handleAction` callback with one of the specified user actions (e.g. `retry`).
    /// The `UserAction`s has to be defined in the project.
    /// - Parameters:
    ///   - router: Instance of class (probably router) which conforms to `ErrorHandlingRouter`,
    ///   - handleAction: Callback through which the router instance can pass user action (e.g. tap on alert button).
    /// - Returns: Chain continuation.
    func handleError(with router: ErrorHandlingRouter,
                     handleAction: @escaping (UserAction) -> Void) -> AnyPublisher<Self.Output, Error> {
        tryCatch { [weak router] error -> Self in
            try router?.handleError(error, handleAction: handleAction)
            throw error
        }
        .eraseToAnyPublisher()
    }
}
