//
//  UserAction.swift
//  
//
//  Created by Lukáš Růžička on 30.04.2021.
//

/// Define custom user action, which can the error handling emit. Using enum would probably be the best option.
public protocol UserAction {}
