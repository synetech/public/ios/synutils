//
//  Publisher+FirstValueAsync.swift
//  
//
//  Created by Stepan Kloucek on 13.10.2022.
//

import Foundation
import Combine

/// This extension is based on this blog post: https://www.swiftbysundell.com/articles/connecting-async-await-with-other-swift-code/
@available(iOS 13.0, *)
public extension Publisher {

    /// - Returns: The first value the Publisher emits. All Future events are ignored.
    func firstValueAsync() async -> Output where Failure == Never {
        var cancellable: AnyCancellable?
        var didReceiveValue = false

        return await withCheckedContinuation { continuation in
            cancellable = sink { value in
                guard !didReceiveValue else { return }

                didReceiveValue = true
                // cancel the cancellable to prevent receiving another value
                cancellable?.cancel()
                continuation.resume(returning: value)
            }
        }
    }

    /// - Returns: The first value the Publisher emits.. All Future events are ignored.
    func firstValueAsync() async throws -> Output {
        var cancellable: AnyCancellable?
        var didReceiveValue = false

        return try await withCheckedThrowingContinuation { continuation in
            cancellable = sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        continuation.resume(throwing: error)
                    case .finished:
                        if !didReceiveValue {
                            continuation.resume(throwing: Publishers.MissingOutputError())
                        }
                    }
                },
                receiveValue: { value in
                    guard !didReceiveValue else { return }

                    didReceiveValue = true
                    cancellable?.cancel()
                    continuation.resume(returning: value)
                }
            )

        }
    }
}

@available(iOS 13.0, *)
public extension Publishers {
    struct MissingOutputError: Error {}
}
