//
//  Publisher+HandleLoading.swift
//  
//
//  Created by Lukáš Růžička on 30.04.2021.
//

import Combine

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension Publisher {

    /// This chain modifier takes care of changing the `isLoading` value according to state of the `Publisher`.
    /// - Parameters:
    ///   - model: Model in which the `isLoading` value is located (probably view model).
    ///   - isLoadingKeyPath: Keypath to the value in model.
    /// - Returns: Chain continuation.
    func handleLoading<Model>(in model: Model, _ isLoadingKeyPath: WritableKeyPath<Model, Bool>)
    -> Publishers.HandleEvents<Self> where Model: AnyObject {
        let startLoading: () -> Void = { [weak model] in
            if !(model?[keyPath: isLoadingKeyPath] ?? false) {
                model?[keyPath: isLoadingKeyPath] = true
            }
        }
        let stopLoading: () -> Void = { [weak model] in
            if model?[keyPath: isLoadingKeyPath] ?? false {
                model?[keyPath: isLoadingKeyPath] = false
            }
        }
        return handleEvents(receiveSubscription: { _ in startLoading() },
                     receiveOutput: { _ in stopLoading() },
                     receiveCompletion: { _ in stopLoading() },
                     receiveCancel: { stopLoading() },
                     receiveRequest: { _ in startLoading() })
    }
}
