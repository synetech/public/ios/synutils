//
//  SubscribersCompletion+IsSuccessful.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import Combine

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension Subscribers.Completion {

    var isSuccess: Bool {
        switch self {
        case .finished:
            return true
        case .failure:
            return false
        }
    }

    var isFailure: Bool {
        switch self {
        case .finished:
            return false
        case .failure:
            return true
        }
    }
}
