//
//  ConnectionType.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

import Foundation

public enum ConnectionType {

    case wifi
    case cellular
    case other
}
