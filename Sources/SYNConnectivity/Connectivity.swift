//
//  Connectivity.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

// NOTE: Inspiration https://stackoverflow.com/questions/30743408/check-for-internet-connection-with-swift
import Combine
import Network

public final class ConnectivityService: ObservableObject {

    // MARK: - Properties
    @Published public var reachabilityInfos: NWPath?
    /// Returns whether the device is connected to internet
    @Published public var isNetworkAvailable: Bool?
    /// Returns type of connection (case of `ConnectionType` enum) which indicates whether user is connected through wifi, mobile data or has no connection.
    @Published public var typeOfCurrentConnection: ConnectionType?

    private let monitor = NWPathMonitor()
    private let backgroundQueue = DispatchQueue.global(qos: .background)

    // MARK: - Init
    public init() {
        setUp()
    }

    deinit {
        monitor.cancel()
    }
}

private extension ConnectivityService {

    func setUp() {
        monitor.pathUpdateHandler = { [weak self] path in
            self?.reachabilityInfos = path
            switch path.status {
            case .satisfied:
                self?.isNetworkAvailable = true
            case .unsatisfied, .requiresConnection:
                self?.isNetworkAvailable = false
            @unknown default:
                self?.isNetworkAvailable = false
            }

            if path.usesInterfaceType(.wifi) {
                self?.typeOfCurrentConnection = .wifi
            } else if path.usesInterfaceType(.cellular) {
                self?.typeOfCurrentConnection = .cellular
            } else if path.usesInterfaceType(.other) {
                self?.typeOfCurrentConnection = .other
            }
        }

        monitor.start(queue: backgroundQueue)
    }
}
