//
//  Date+ComparingDateComponent.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import Foundation

public extension Date {

    // MARK: - Subtypes
    enum TimeDirection {

        case ago
        case inFuture
    }

    enum CompareOperator {

        case lessThan
        case lessThanOrEqual
        case moreThanOrEqual
        case moreThan
    }

    enum KnownCalendarComponent {

        case second
        case minute
        case hour
        case day
        case month
        case year
    }

    // MARK: - Comparers
    /// Compares date to the current one in given date component.
    /// It can be used like this: `myDate.isLess(than: 1, .hour, .ago)`
    /// - Warning: Now only seconds, minutes, hours, days, months and years are supported!
    /// - Parameters:
    ///   - value: Amount of given component
    ///   - component: Date component like `.hour` or `.day`.
    ///   - timeDirection: Whether it should be compared to the past (`.ago`) or the future (`.inFuture`).
    /// - Returns: The comparation result
    func isLess(than value: Int, _ component: KnownCalendarComponent, _ timeDirection: TimeDirection) -> Bool {
        isDate(.lessThan, value, component, timeDirection)
    }

    /// Compares date to the current one in given date component.
    /// It can be used like this: `myDate.isLessOrEqual(than: 1, .hour, .ago)`
    /// - Warning: Now only seconds, minutes, hours, days, months and years are supported!
    /// - Parameters:
    ///   - value: Amount of given component
    ///   - component: Date component like `.hour` or `.day`.
    ///   - timeDirection: Whether it should be compared to the past (`.ago`) or the future (`.inFuture`).
    /// - Returns: The comparation result
    func isLessOrEqual(than value: Int, _ component: KnownCalendarComponent, _ timeDirection: TimeDirection) -> Bool {
        isDate(.lessThanOrEqual, value, component, timeDirection)
    }

    /// Compares date to the current one in given date component.
    /// It can be used like this: `myDate.isMoreOrEqual(than: 1, .hour, .ago)`
    /// - Warning: Now only seconds, minutes, hours, days, months and years are supported!
    /// - Parameters:
    ///   - value: Amount of given component
    ///   - component: Date component like `.hour` or `.day`.
    ///   - timeDirection: Whether it should be compared to the past (`.ago`) or the future (`.inFuture`).
    /// - Returns: The comparation result
    func isMoreOrEqual(than value: Int, _ component: KnownCalendarComponent, _ timeDirection: TimeDirection) -> Bool {
        isDate(.moreThanOrEqual, value, component, timeDirection)
    }

    /// Compares date to the current one in given date component.
    /// It can be used like this: `myDate.isMore(than: 1, .hour, .ago)`
    /// - Warning: Now only seconds, minutes, hours, days, months and years are supported!
    /// - Parameters:
    ///   - value: Amount of given component
    ///   - component: Date component like `.hour` or `.day`.
    ///   - timeDirection: Whether it should be compared to the past (`.ago`) or the future (`.inFuture`).
    /// - Returns: The comparation result
    func isMore(than value: Int, _ component: KnownCalendarComponent, _ timeDirection: TimeDirection) -> Bool {
        isDate(.moreThan, value, component, timeDirection)
    }
}

// MARK: - Comparer implementation
private extension Date {

    /// Compares date to the current one in given date component.
    /// It can be used like this: `myDate.isDate(.lessThan, 1, .hour, .ago)`
    /// - Warning: Now only seconds, minutes, hours, days, months and years are supported!
    /// - Parameters:
    ///   - compareOperator: `lessThan`, `lessThanOrEqual`, `moreThanOrEqual` or `moreThan`
    ///   - value: Amount of given component
    ///   - component: Date component like `.hour` or `.day`.
    ///   - timeDirection: Whether it should be compared to the past (`.ago`) or the future (`.inFuture`).
    /// - Returns: The comparation result
    func isDate(_ compareOperator: CompareOperator, _ value: Int,
                _ component: KnownCalendarComponent, _ timeDirection: TimeDirection) -> Bool {
        let componentValue = timeDirection.get(component, for: self) ?? 0
        return compareOperator.compare(componentValue, and: value)
    }
}

// MARK: - TimeDirection get component value
private extension Date.TimeDirection {

    func get(_ component: Date.KnownCalendarComponent, for date: Date) -> Int? {
        var fromDate = date
        var toDate = Date()
        if self == .inFuture {
            fromDate = Date()
            toDate = date
        }
        switch component {
        case .second:
            return Calendar.current.dateComponents([.second], from: fromDate, to: toDate).second
        case .minute:
            return Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute
        case .hour:
            return Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour
        case .day:
            return Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day
        case .month:
            return Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month
        case .year:
            return Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year
        }
    }
}

// MARK: - CompareOperator compare function
private extension Date.CompareOperator {

    func compare(_ computedValue: Int, and comparedValue: Int) -> Bool {
        switch self {
        case .lessThan:
            return computedValue < comparedValue
        case .lessThanOrEqual:
            return computedValue <= comparedValue
        case .moreThanOrEqual:
            return computedValue >= comparedValue
        case .moreThan:
            return computedValue > comparedValue
        }
    }
}
