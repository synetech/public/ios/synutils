//
//  Date+ComparingOtherDate.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

import Foundation

public extension Date {

    /// Compares two dates with adding different units to given date.
    /// - Parameters:
    ///   - value: Amount of the given unit.
    ///   - unit: Calendar unit. Case from `Calendar.Component` (e.g. `.minute` or `.day`)
    ///   - initialDate: Date to which the given values are added.
    /// - Returns: Whether the called date is less than the given date with added value.
    func isLessThan(_ value: Int, unit: Calendar.Component, from initialDate: Date) -> Bool {
        guard let compareResult = compareSelf(to: initialDate, withAdded: value, unit: unit) else { return false }
        return compareResult == .orderedAscending
    }

    /// Compares two dates with adding different units to given date.
    /// - Parameters:
    ///   - value: Amount of the given unit.
    ///   - unit: Calendar unit. Case from `Calendar.Component` (e.g. `.minute` or `.day`)
    ///   - initialDate: Date to which the given values are added.
    /// - Returns: Whether the called date is less than or equal to the given date with added value.
    func isLessThanOrEqual(_ value: Int, unit: Calendar.Component, from initialDate: Date) -> Bool {
        guard let compareResult = compareSelf(to: initialDate, withAdded: value, unit: unit) else { return false }
        return compareResult == .orderedAscending || compareResult == .orderedSame
    }

    /// Compares two dates with adding different units to given date.
    /// - Parameters:
    ///   - value: Amount of the given unit.
    ///   - unit: Calendar unit. Case from `Calendar.Component` (e.g. `.minute` or `.day`)
    ///   - initialDate: Date to which the given values are added.
    /// - Returns: Whether the called date is greater than the given date with added value.
    func isGreaterThan(_ value: Int, unit: Calendar.Component, from initialDate: Date) -> Bool {
        guard let compareResult = compareSelf(to: initialDate, withAdded: value, unit: unit) else { return false }
        return compareResult == .orderedDescending
    }

    /// Compares two dates with adding different units to given date.
    /// - Parameters:
    ///   - value: Amount of the given unit.
    ///   - unit: Calendar unit. Case from `Calendar.Component` (e.g. `.minute` or `.day`)
    ///   - initialDate: Date to which the given values are added.
    /// - Returns: Whether the called date is greater than or equal to the given date with added value.
    func isGreaterThanOrEqual(_ value: Int, unit: Calendar.Component, from initialDate: Date) -> Bool {
        guard let compareResult = compareSelf(to: initialDate, withAdded: value, unit: unit) else { return false }
        return compareResult == .orderedDescending || compareResult == .orderedSame
    }

    /// Returns info whether date is today
    /// - Returns: Whether the called date is today
    var isToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
}

// MARK: - Supporting functions
private extension Date {

    func compareSelf(to date: Date, withAdded value: Int, unit: Calendar.Component) -> ComparisonResult? {
        let calendar = Calendar.current
        guard let targetDate = calendar.date(byAdding: unit, value: value, to: date) else { return nil }
        return calendar.compare(self, to: targetDate, toGranularity: unit)
    }
}
