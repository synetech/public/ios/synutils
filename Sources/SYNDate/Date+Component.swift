//
//  Date+Component.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import Foundation

public extension Date {
    
    func getComponent(_ component: Calendar.Component) -> Int {
        Calendar.current.component(component, from: self)
    }
}
