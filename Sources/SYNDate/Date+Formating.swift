//
//  Date+Formating.swift
//
//  Most common date formats converting to string
//  To test new formats or for reference see https://nsdateformatter.com
//
//  Created by Štěpán Klouček on 15.03.2021.
//

import Foundation

public extension Date {
    
    /// - Returns: Formated date in dd. MM. yyyy, HH:mm format
    var dateAndTime: String {
        return formatString("dd. MM. yyyy, HH:mm")
    }
    
    /// - Returns: Formated date in dd. MM. yyyy format
    var dateString: String {
        return formatString("dd. MM. yyyy")
    }
    
    /// - Returns: Formated date in HH:mm format
    var dayTime: String {
        return formatString("HH:mm")
    }
    
    /// - Returns: Formated date in dd format
    var dayOfMonth: String {
        return formatString("dd")
    }
    
    /// Convinient date formating
    /// - Parameters:
    ///   - format: Format string
    /// - Returns: Formated string
    func formatString(_ format: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format
        return dateFormater.string(from: self)
    }
}
