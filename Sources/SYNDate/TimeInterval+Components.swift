//
//  TimeInterval+Components.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import Foundation

public extension TimeInterval {

    static var minute: Self { 60 }
    static var hour: Self { 60 * minute }
    static var day: Self { 24 * hour }
    static var week: Self { 7 * day }
}
