//
//  FontPackage.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

#if os(iOS)
import UIKit

public struct FontPackage {

    // MARK: - Properties
    let regularFont: UIFont
    let boldFont: UIFont
    let italicFont: UIFont

    // MARK: - Init
    public init(regularFont: UIFont, boldFont: UIFont, italicFont: UIFont) {
        self.regularFont = regularFont
        self.boldFont = boldFont
        self.italicFont = italicFont
    }
}

public extension FontPackage {
    
    /// Based on provided font's type (regular, bold, italic), return desired font counterpart
    func getTargetFont(for font: UIFont) -> UIFont {
        if font.hasTrait(.traitBold) {
            return boldFont
        }
        if font.hasTrait(.traitItalic) {
            return italicFont
        }
        return regularFont
    }
}
#endif
