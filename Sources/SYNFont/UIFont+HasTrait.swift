//
//  UIFont+HasTrait.swift
//  AnonymousAccess
//
//  Created by Tomáš Novotný on 07/08/2019.
//  Copyright © 2019 synetech. All rights reserved.
//

#if os(iOS)
import UIKit.UIFont

public extension UIFont {

    /// Return true if self contains specified trait
    /// Example: hasTrait(.traitBold) returns true if the font is bold
    func hasTrait(_ trait: UIFontDescriptor.SymbolicTraits) -> Bool {
        return fontDescriptor.symbolicTraits.contains(trait)
    }
}
#endif
