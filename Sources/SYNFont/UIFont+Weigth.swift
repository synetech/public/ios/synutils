////
//  UIFont+Weigth.swift
//  BezpecnyDomov
//
//  Created by Vojtěch Pajer on 03/11/2020.
//  Copyright © 2020 SYNETECH s.r.o. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UIFont {

    /// Sets a custom weigth to a font.
    /// Use when design resources specify font-weight as number instead of a literal value (normal, bold, etc.)
    /// The values are:
    ///  - ultraLight: 100
    ///  - thin: 200
    ///  - light: 300
    ///  - regular: 400 = normal font
    ///  - medium: 500
    ///  - semibold: 600
    ///  - bold: 700
    ///  - heavy: 800
    ///  - black: 900
    ///
    /// Majority of fonts does not have all variants of the font. 
    func withWeigth(_ weigth: UIFont.Weight) -> UIFont {
        let weigthDescriptor = fontDescriptor
            .addingAttributes([.traits: [UIFontDescriptor.TraitKey.weight: weigth]])
        return UIFont(descriptor: weigthDescriptor, size: pointSize)
    }
}
#endif
