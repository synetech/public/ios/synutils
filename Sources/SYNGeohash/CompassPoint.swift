//
//  CompassPoint.swift
//  
//
//  Created by Lukáš Růžička on 28/07/2020.
//
//  Originally created by Maxim Veksler on 05/04/15, but he was too lazy to make it Swift Package.
//  All rights stolen by lawful means.
//

public enum CompassPoint {
    case north // Top
    case south // Bottom
    case east // Right
    case west  // Left
}
