//
//  Geohash.swift
//  
//
//  Created by Lukáš Růžička on 28/07/2020.
//
//  Originally created by Maxim Veksler on 05/04/15, but he was too lazy to make it Swift Package.
//  All rights stolen by lawful means.
//

//swiftlint:disable identifier_name
public struct Geohash {

    public static let defaultPrecision = 5

    private static let DecimalToBase32Map = Array("0123456789bcdefghjkmnpqrstuvwxyz") // decimal to 32base mapping (0 => "0", 31 => "z")
    private static let Base32BitflowInit: UInt8 = 0b10000

    // - MARK: Public
    public static func encode(latitude: Double, longitude: Double, _ precision: Int = Geohash.defaultPrecision) -> String? {
        return geohashBox(latitude: latitude, longitude: longitude, precision)?.hash
    }

    public static func decode(_ hash: String) -> (latitude: Double, longitude: Double)? {
        return geohashBox(hash)?.point
    }

    public static func neighbour(_ centerHash: String, direction: CompassPoint) -> String? {
        guard let box = geohashBox(centerHash) else { return nil }

        // neighbor precision *must* be them same as center'ed bounding box.
        let precision = centerHash.count

        switch direction {
        case .north:
            let newLatitude = box.point.latitude + box.size.latitude // North is upper in the latitude scale
            return geohashBox(latitude: newLatitude, longitude: box.point.longitude, precision)?.hash
        case .south:
            let newLatitude = box.point.latitude - box.size.latitude // South is lower in the latitude scale
            return geohashBox(latitude: newLatitude, longitude: box.point.longitude, precision)?.hash
        case .east:
            var newLongitude = box.point.longitude + box.size.longitude // East is bigger in the longitude scale
            if newLongitude > 180 { // Crossing date line
                newLongitude = -box.point.longitude
            }
            return geohashBox(latitude: box.point.latitude, longitude: newLongitude, precision)?.hash
        case .west:
            var newLongitude = box.point.longitude - box.size.longitude // West is lower in the longitude scale
            if newLongitude < -180 { // Crossing date line
                newLongitude = -box.point.longitude
            }
            return geohashBox(latitude: box.point.latitude, longitude: newLongitude, precision)?.hash
        }
    }

    // - MARK: Private
    private static func geohashBox(latitude: Double, longitude: Double, _ precision: Int = Geohash.defaultPrecision) -> GeohashBox? {
        var lat = (-90.0, 90.0)
        var lon = (-180.0, 180.0)

        // to be generated result.
        var geohash = String()

        // Loop helpers
        var parityMode = Parity.even
        var base32char = 0
        var bit = Base32BitflowInit

        repeat {
            switch parityMode {
            case .even:
                let mid = (lon.0 + lon.1) / 2
                if longitude >= mid {
                    base32char |= Int(bit)
                    lon.0 = mid
                } else {
                    lon.1 = mid
                }
            case .odd:
                let mid = (lat.0 + lat.1) / 2
                if latitude >= mid {
                    base32char |= Int(bit)
                    lat.0 = mid
                } else {
                    lat.1 = mid
                }
            }

            // Flip between Even and Odd
            parityMode = !parityMode
            // And shift to next bit
            bit >>= 1

            if bit == 0b00000 {
                geohash += String(DecimalToBase32Map[base32char])
                bit = Base32BitflowInit // set next character round.
                base32char = 0
            }
            
        } while geohash.count < precision

        return GeohashBox(hash: geohash, north: lat.1, west: lon.0, south: lat.0, east: lon.1)
    }

    private static func geohashBox(_ hash: String) -> GeohashBox? {
        var parityMode = Parity.even
        var lat = (-90.0, 90.0)
        var lon = (-180.0, 180.0)

        for c in hash {
            guard let bitmap = DecimalToBase32Map.firstIndex(of: c) else {
                // Break on non geohash code char.
                return nil
            }

            var mask = Int(Base32BitflowInit)
            while mask != 0 {

                switch parityMode {
                case .even:
                    if bitmap & mask != 0 {
                        lon.0 = (lon.0 + lon.1) / 2
                    } else {
                        lon.1 = (lon.0 + lon.1) / 2
                    }
                case .odd:
                    if bitmap & mask != 0 {
                        lat.0 = (lat.0 + lat.1) / 2
                    } else {
                        lat.1 = (lat.0 + lat.1) / 2
                    }
                }

                parityMode = !parityMode
                mask >>= 1
            }
        }

        return GeohashBox(hash: hash, north: lat.1, west: lon.0, south: lat.0, east: lon.1)
    }
}
