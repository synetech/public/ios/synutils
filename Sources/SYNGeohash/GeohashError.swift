//
//  GeohashError.swift
//  
//
//  Created by Lukáš Růžička on 27/08/2020.
//

import SYNIdentifiers

public enum GeohashError: IdentifiableError {

    case encodingFailed
}
