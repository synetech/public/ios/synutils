//
//  GeohashRegion.swift
//  
//
//  Created by Lukáš Růžička on 27/08/2020.
//

/// Serves as a model for region made by geohashes with given precision and edge locations
public struct GeohashRegion {

    // MARK: - Properties
    let topLeft: String
    let topRight: String
    let bottomLeft: String

    // MARK: - Init
    /// Inits the region from coordinates.
    /// - Parameters:
    ///   - minLatitude: Minimal latitude of the area.
    ///   - maxLatitude: Maximal latitude of the area.
    ///   - minLongitude: Minimal longitude of the area.
    ///   - maxLongitude: Maximal longitude of the area.
    ///   - precision: Optional precision of the geohash. Default is 5.
    /// - Throws: `GeohashError` if the encoding fails.
    public init(minLatitude: Double, maxLatitude: Double,
                minLongitude: Double, maxLongitude: Double,
                precision: Int = 5) throws {
        guard let topLeft = Geohash.encode(latitude: maxLatitude, longitude: minLongitude, precision),
            let topRight = Geohash.encode(latitude: maxLatitude, longitude: maxLongitude, precision),
            let bottomLeft = Geohash.encode(latitude: minLatitude, longitude: minLongitude, precision)
            else { throw GeohashError.encodingFailed }
        self.topLeft = topLeft
        self.topRight = topRight
        self.bottomLeft = bottomLeft
    }

    /// Inits the region from the geohash corners. The precision is given by precision of the parameters. It always needs to be same for all parameters!
    /// - Parameters:
    ///   - topLeft: Geohash for top left corner of the area.
    ///   - topRight: Geohash for top right corner of the area.
    ///   - bottomLeft: Geohash for bottom left corner of the area.
    public init(topLeft: String, topRight: String, bottomLeft: String) {
        self.topLeft = topLeft
        self.topRight = topRight
        self.bottomLeft = bottomLeft
    }
}

// MARK: - Geohashes calculating
public extension GeohashRegion {

    /// Calculates all geohases in the region defined by the model instance. The precision is set by the model (default precision is 5).
    func getAllGeohashes() -> [String] {
        var geohashes: [String] = []

        var currentX = topLeft
        guard let targetX = Geohash.neighbour(topRight, direction: .east) else { return [] }
        var currentY = currentX
        var targetY = bottomLeft

        while currentX != targetX {
            geohashes.append(currentX)

            while currentY != targetY {
                guard let nextY = Geohash.neighbour(currentY, direction: .south) else { break }
                currentY = nextY
                geohashes.append(currentY)

                // Fallback to avoid infinite loop
                guard geohashes.count <= 300 else { break }
            }

            guard let nextX = Geohash.neighbour(currentX, direction: .east),
                let nextTargetY = Geohash.neighbour(targetY, direction: .east) else { break }
            currentX = nextX
            currentY = nextX
            targetY = nextTargetY

            // Fallback to avoid infinite loop
            guard geohashes.count <= 300 else { break }
        }

        return geohashes
    }
}
