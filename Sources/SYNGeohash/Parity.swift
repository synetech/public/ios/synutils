//
//  Parity.swift
//  
//
//  Created by Lukáš Růžička on 28/07/2020.
//
//  Originally created by Maxim Veksler on 05/04/15, but he was too lazy to make it Swift Package.
//  All rights stolen by lawful means.
//

//swiftlint:disable identifier_name
enum Parity {
    case even, odd
}

prefix func ! (a: Parity) -> Parity {
    return a == .even ? .odd : .even
}
