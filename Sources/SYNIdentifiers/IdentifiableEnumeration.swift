//
//  IdentifiableEnumeration.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

import Foundation
import SYNString

/// Conform your enum to String and this protocol to use the `identifier`, which returns the enum name,
/// and the `rawValue`, which returns the enum name with the enumeration case.
public protocol IdentifiableEnumeration {

    associatedtype EnumType

    var identifier: String { get }
    var rawValue: EnumType { get }
    var caseIdentifier: String { get }
}

public extension IdentifiableEnumeration where Self.EnumType == String {

    /// Returns the first-character-lowercased name of the Enum class (e.g. tabBar)
    var identifier: String {
        return String(describing: type(of: self)).lowercaseFirstCharacter()
    }

    /// Returns the whole identifier of the enumeration case (e.g. tabBarView)
    var rawValue: String {
        return identifier + String(describing: self).capitalizeFirstCharacter()
    }

    /// Return only the name of the enum without associated values
    var caseIdentifier: String {
        let mirror = Mirror(reflecting: self)
        if let label = mirror.children.first?.label {
            return label
        } else {
            return String(describing: self)
        }
    }
}
