//
//  IdentifiableError.swift
//  
//
//  Created by Lukáš Růžička on 27/08/2020.
//

import Foundation

public protocol IdentifiableError: LocalizedError, IdentifiableEnumeration where EnumType == String {}

public extension IdentifiableError {

    var errorDescription: String? {
        return rawValue
    }
}
