//
//  NestedIdentifiableEnumeration.swift
//  
//
//  Created by Lukáš Růžička on 27/08/2020.
//

import Foundation
import SYNString

/// You need to conform your enum to String and this protocol and set the typealias for `SuperType` (the type of the enums parent). Then you can use:
/// - `superTypeIdentifier`, which returns the super type name
/// - `identifier`, which returns the enum name with super type name in prefix
/// - `rawValue`, which returns the enum name with the enumeration case with super type name in prefix
public protocol NestedIdentifiableEnumeration: IdentifiableEnumeration {
    associatedtype SuperType: IdentifiableEnumeration
}

// MARK: - Identifier with super type
public extension NestedIdentifiableEnumeration where Self.EnumType == String {

    /// Returns the super type name (e.g. mainScreen)
    var superTypeIdentifier: String {
        return String(describing: type(of: SuperType.self)).lowercaseFirstCharacter().removeTypeSuffix()
    }

    /// Returns the super type name and name of the enum class (e.g. mainScreenFirstSection)
    var identifier: String {
        return superTypeIdentifier
            + String(describing: type(of: self)).capitalizeFirstCharacter()
    }

    /// Returns the whole identifier of the enumeration case with super type name in prefix (e.g. mainScreenFirstSectionView)
    var rawValue: String {
        return superTypeIdentifier
            + String(describing: type(of: self)).capitalizeFirstCharacter()
            + String(describing: self).capitalizeFirstCharacter()
    }
}

// MARK: - Necessary String extensions
fileprivate extension String {

    func removeTypeSuffix() -> String {
        return replacingOccurrences(of: ".Type", with: "")
    }
}
