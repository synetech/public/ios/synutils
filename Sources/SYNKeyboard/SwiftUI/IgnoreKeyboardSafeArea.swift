//
//  IgnoreKeyboardSafeArea.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(SwiftUI)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    func ignoreKeyboardSafeArea() -> some View {
        modifier(IgnoreKeyboardSafeArea())
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private struct IgnoreKeyboardSafeArea: ViewModifier {

    @ViewBuilder
    func body(content: Content) -> some View {
        if #available(iOS 14.0, OSX 11.0, tvOS 14.0, watchOS 7.0, *) {
            content
                .ignoresSafeArea(.keyboard)
        } else {
            content
        }
    }
}

#endif
