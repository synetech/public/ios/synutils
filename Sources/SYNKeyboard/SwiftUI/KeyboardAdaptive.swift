//
//  KeyboardAdaptive.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//
// NOTE: - This is inspired by solution from Vadim Bulavin
// [https://www.vadimbulavin.com/how-to-move-swiftui-view-when-keyboard-covers-text-field/].
//

#if canImport(UIKit)
import Combine
import Introspect
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    /// This will make sure that all the passed text fields (just the active one at a time)
    /// and views will be visibile when keyboard appears.
    /// - Note: Since iOS 14, the views tries to adapt to keyboard itselves.
    /// To prevent double keyboard offset handling, use `ignoreKeyboardSafeArea`
    /// (or  directly`ignoresSafeArea(.keyboard)`) on the whole screen view.
    /// If you use the modifier in same chain, make sure that this one precede the ignore of keyboard safe area.
    /// - Parameters:
    ///   - followingFieldMargin: It declares how much the text field should offset to make
    ///   the following field visible. Default is `40`
    ///   - fieldsContainerType: If set to `scrollView` the scroll view will remain whole above the keyboard
    ///   and the content will scroll to make the active fields visible.
    ///   It ensures that user can still scroll to the bottom even if the keyboard is shown.
    ///   Default is `staticView`.
    func makeKeyboardAdaptive(followingFieldMargin: CGFloat = 40,
                              fieldsContainerType: KeyboardAdaptive.FieldsContainerType = .staticView) -> some View {
        modifier(KeyboardAdaptive(followingFieldMargin: followingFieldMargin,
                                  fieldsContainerType: fieldsContainerType))
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct KeyboardAdaptive: ViewModifier {

    // MARK: - Subtypes
    public enum FieldsContainerType: Equatable {

        case staticView
        /// This property should indicate whether this modifier is applied directly to the scroll view (`true`) or
        /// if the modifier is applied to other view which wraps the scroll view.
        case scrollView(isAppliedDirectly: Bool)
    }

    // MARK: - Properties
    let followingFieldMargin: CGFloat
    let fieldsContainerType: FieldsContainerType

    @State private var keyboardPadding: CGFloat = 0

    // MARK: - Body
    public func body(content: Content) -> some View {
        GeometryReader { proxy in
            content
                .padding(.top, fieldsContainerType == .staticView ? -keyboardPadding: 0)
                .padding(.bottom, keyboardPadding)
                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                    guard keyboardHeight != 0 else {
                        keyboardPadding = 0
                        return
                    }
                    let overlapInViewHeight = getHeightOfKeyboardOverlapInView(proxy: proxy,
                                                                               keyboardHeight: keyboardHeight)
                    switch fieldsContainerType {
                    case .staticView:
                        let overlapOfActiveField = getKeyboardOverlapOfFirstResponder(
                            proxy: proxy, overlapHeight: overlapInViewHeight)
                        keyboardPadding = max(0, overlapOfActiveField)
                    case .scrollView:
                        keyboardPadding = overlapInViewHeight
                    }
                }
                .introspectScrollView(customize: { scrollView in
                    guard case .scrollView = fieldsContainerType, keyboardPadding != 0 else { return }
                    let frameHeight = fieldsContainerType.isAppliedToScrollViewFrame
                        ? proxy.size.height : scrollView.frame.height
                    guard let contentOffsetY = getContentOffsetYForFirstResponder(
                            inScrollView: scrollView, frameHeight: frameHeight) else { return }
                    scrollView.setContentOffset(CGPoint(x: 0, y: contentOffsetY), animated: true)
                })
        }
        .animation(keyboardPadding > 0 ? .easeOut : nil)
    }
}

// MARK: - Coordinates calculations
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private extension KeyboardAdaptive {

    /// Returns height of container view's area which is overlaped by the keyboard.
    /// - Parameters:
    ///   - proxy: `GeometryProxy` for the container view
    ///   - keyboardHeight: Full keyboard height
    /// - Returns: Height of container view's area which is overlaped by the keyboard
    func getHeightOfKeyboardOverlapInView(proxy: GeometryProxy, keyboardHeight: CGFloat) -> CGFloat {
        let viewToScreenBottom = UIScreen.main.bounds.height - proxy.frame(in: .global).maxY
        return keyboardHeight - viewToScreenBottom
    }

    /// Returns how much the keyboard overlaps the first responder (active field).
    /// It's the difference between first responder's bottom and keyboard top.
    /// Includes the `followingFieldMargin` value..
    /// - Parameters:
    ///   - proxy: `GeometryProxy` for the container view
    ///   - overlapHeight: Height of container view's area which is overlaped by the keyboard
    /// - Returns: Difference between first responder's bottom and keyboard top.
    /// Includes the `followingFieldMargin` value.
    func getKeyboardOverlapOfFirstResponder(proxy: GeometryProxy, overlapHeight: CGFloat) -> CGFloat {
        let keyboardTop = proxy.size.height - overlapHeight
        let firstResponderBottom = (UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0)
            - proxy.frame(in: .global).minY
        return (firstResponderBottom + followingFieldMargin) - keyboardTop
    }

    /// Return scroll content offset y value for the first responder (active field),
    /// including the `followingFieldMargin` value.
    /// When the offset aplied the first responder bottom is aligned to bottom of the visible frame of scrollview.
    /// - Parameters:
    ///   - scrollView: The scrollview which contains the first responder.
    ///   - frameHeight: Height of the scroll view.
    /// - Returns: Content offset Y value.
    func getContentOffsetYForFirstResponder(inScrollView scrollView: UIScrollView,
                                            frameHeight: CGFloat) -> CGFloat? {
        guard let firstResponderView = UIResponder.currentFirstResponder as? UIView else { return nil }
        let firstResponderFrameInScrollContent = firstResponderView
            .convert(firstResponderView.frame, to: scrollView)
        let scrollViewHeightWithShownKeyboard = frameHeight - keyboardPadding
        let offsetY = firstResponderFrameInScrollContent.maxY + followingFieldMargin -
            scrollViewHeightWithShownKeyboard
        guard offsetY > 0 else { return nil }
        return offsetY
    }
}

// MARK: - Field container type computed variables
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private extension KeyboardAdaptive.FieldsContainerType {

    var isAppliedToScrollViewFrame: Bool {
        if case .scrollView(let isAppliedDirectly) = self {
            return isAppliedDirectly
        }
        return false
    }
}

#endif
