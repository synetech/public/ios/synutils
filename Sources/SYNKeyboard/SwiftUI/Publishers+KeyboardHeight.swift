//
//  Publishers+KeyboardHeight.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import Combine
import UIKit

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension Publishers {

    static var keyboardHeight: AnyPublisher<CGFloat, Never> {
        let willShow = NotificationCenter.default.publisher(for: UIApplication.keyboardWillShowNotification)
            .map { $0.keyboardHeight }
        let willHide = NotificationCenter.default.publisher(for: UIApplication.keyboardWillHideNotification)
            .map { _ in CGFloat(0) }

        return MergeMany(willShow, willHide)
            .eraseToAnyPublisher()
    }
}

// MARK: - Keyboard height from notification
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private extension Notification {

    var keyboardHeight: CGFloat {
        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
    }
}

#endif
