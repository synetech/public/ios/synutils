//
//  UIResponder+GlobalFrame.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIResponder {

    var globalFrame: CGRect? {
        guard var view = self as? UIView else { return nil }
        if view.frame == .zero,
           let nonZeroFrameParentView = view.getFirstNonZeroFrameParent() {
            view = nonZeroFrameParentView
        }
        return view.convert(view.frame, to: nil)
    }
}

#endif
