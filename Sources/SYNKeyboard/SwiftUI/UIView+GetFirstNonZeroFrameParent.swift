//
//  UIView+GetFirstNonZeroFrameParent.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIView {

    func getFirstNonZeroFrameParent() -> UIView? {
        guard frame == .zero else { return self }
        return superview?.getFirstNonZeroFrameParent()
    }
}

#endif
