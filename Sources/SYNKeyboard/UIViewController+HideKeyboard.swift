//
//  UIViewController+HideKeyboard.swift
//  DentVIS
//
//  Created by Lukáš Růžička on 22/04/2020.
//  Copyright © 2020 OrtoDUO. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UIViewController {

    func hideKeyboardWhenTapped(in view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
#endif
