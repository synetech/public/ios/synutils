//
//  Numbers+StringFormatting.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

import Foundation

public extension Double {

    /// Fomats the value to `String` with given ammount of decimals.
    /// - Parameter decimals: Decimal ammount.
    /// - Returns: Formatted `String`.
    func roundedString(to decimals: Int) -> String {
        return String(format: "%.\(decimals)f", self)
    }
}

public extension Float {

    /// Fomats the value to `String` with given ammount of decimals.
    /// - Parameter decimals: Decimal ammount.
    /// - Returns: Formatted `String`.
    func roundedString(to decimals: Int) -> String {
        return String(format: "%.\(decimals)f", self)
    }
}
