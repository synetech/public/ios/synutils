//
//  PKCEPair.swift
//  BezpecnyDomov
//
//  Created by Lukáš Růžička on 12/11/2020.
//  Copyright © 2020 SYNETECH. All rights reserved.
//

import CommonCrypto
import CryptoKit
import Foundation

/// PKCE (pixy) implementation as defined in [RFC7636](https://tools.ietf.org/html/rfc7636)
open class PKCEPair {

    // MARK: - Constants
    private static let allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~"
    private static let allowedRange = 43...128

    // MARK: - Properties
    /// Plain random generated String from allowed characters and in allowed range
    public var verificationCode: String
    /// Hashed and encoded verification code
    public var challenge: String
    /// Hashing method
    public var method = "S256"

    // MARK: - Init
    public init() {
        verificationCode = Self.generateVerificationCode()
        challenge = Self.hashAndEncode(verificationCode)
    }
}

// MARK: - Generating of verification code
private extension PKCEPair {

    static func generateVerificationCode() -> String {
        let randomLength = Int.random(in: allowedRange)
        //swiftlint:disable:next force_unwrapping
        let verificationCode = String((0..<randomLength).map { _ in allowedCharacters.randomElement()! })
        return verificationCode
    }
}

// MARK: - Hashing and encoding challenge
private extension PKCEPair {

    static func hashAndEncode(_ code: String) -> String {
        let codeData = Data(code.utf8)
        let digest: Data
        digest = createSHA256Digest(for: codeData)
        return digest.base64EncodedString()
    }

    static func createSHA256Digest(for data: Data) -> Data {
        var digestData = Data(count: numericCast(CC_SHA256_DIGEST_LENGTH))
        data.withUnsafeBytes { messageBytes in
            digestData.withUnsafeMutableBytes { digestBytes in
                //swiftlint:disable:next force_unwrapping
                let messagePtr = messageBytes.baseAddress!
                //swiftlint:disable:next force_unwrapping
                let digestPtr = digestBytes.baseAddress!.bindMemory(to: UInt8.self, capacity: digestBytes.count)
                _ = CC_SHA256(messagePtr, CC_LONG(data.count), digestPtr)
            }
        }
        return digestData
    }
}
