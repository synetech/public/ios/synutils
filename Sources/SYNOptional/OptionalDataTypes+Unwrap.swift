//
//  OptionalDataTypes+Unwrap.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

import CoreGraphics
import Foundation

// MARK: - Text
public extension Optional where Wrapped == String {
    func unwrap() -> String {
        return self ?? ""
    }
}

public extension Optional where Wrapped == Character {
    func unwrap() -> Character {
        return self ?? " "
    }
}

// MARK: - Numbers
public extension Optional where Wrapped == Int {
    func unwrap() -> Int {
        return self ?? 0
    }
}

public extension Optional where Wrapped == Double {
    func unwrap() -> Double {
        return self ?? 0.0
    }
}

public extension Optional where Wrapped == Float {
    func unwrap() -> Float {
        return self ?? 0.0
    }
}

public extension Optional where Wrapped == CGFloat {
    func unwrap() -> CGFloat {
        return self ?? 0.0
    }
}

// MARK: - Bool
public extension Optional where Wrapped == Bool {
    func falseUnwrap() -> Bool {
        return self ?? false
    }

    func trueUnwrap() -> Bool {
        return self ?? true
    }
}
