//
//  ExternalRouter.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

#if os(iOS)
import SafariServices

protocol ExternalRouter {

    func openURL(_ url: URL)
    func showSafariViewController(with urlString: String)
    func callPhoneNumber(_ number: String)
    func startNewMessageInMailApp(to address: String)
    func share(url: String)
}

class ExternalRouterImpl {}

extension ExternalRouterImpl: ExternalRouter {

    func openURL(_ url: URL) {
        guard UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }

    func callPhoneNumber(_ number: String) {
        guard let url = URL(string: "tel://\(number)") else { return }
        openURL(url)
    }

    func startNewMessageInMailApp(to address: String) {
        guard let url = URL(string: "mailto:\(address)") else { return }
        openURL(url)
    }

    func share(url: String) {
        let activityViewController = UIActivityViewController(activityItems: [url as Any],
                                                              applicationActivities: nil)
        UIApplication.shared.keyWindow?.rootViewController?.present(activityViewController,
                                                                    animated: true,
                                                                    completion: nil)
    }

    func showSafariViewController(with urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let configuration = SFSafariViewController.Configuration()
        configuration.entersReaderIfAvailable = true
        let viewController = SFSafariViewController(url: url, configuration: configuration)
        UIApplication.shared.keyWindow?.rootViewController?.present(viewController, animated: true)
    }
}
#endif
