//
//  Sequence+ParallelMap.swift
//
//  Created by Matěj Bárta on 27.10.2022.
//  Copyright © 2022 SYNETECH. All rights reserved.
//

public extension Sequence {

    /** Works same as compactMap only returns asynchronously transformed elements executed in parallel. Position of elements is preserved.
        - Parameter parallel: If true, sequence will be executed in parallel otherwise serial
        - Parameter transform: Async function returning transformed element
     */
    func asyncCompactMap<T>(parallel: Bool = false, _ transform: @escaping (Element) async throws -> T?) async rethrows -> [T] {
        return try await parallel ? asyncParallelCompactMap(transform) : asyncSerialCompactMap(transform)
    }

    /** Works same as flatMap only returns asynchronously transformed elements executed in parallel. Position of elements is preserved.
         - Parameter parallel: If true, sequence will be executed in parallel otherwise serial
         - Parameter transform: Async function returning transformed element
     */
    func asyncFlatMap<T>(parallel: Bool = false, _ transform: @escaping (Element) async throws -> [T]) async rethrows -> [T] {
        return try await parallel ? asyncParallelFlatMap(transform) : asyncSerialFlatMap(transform)
    }
}

// MARK: Supporting functions
private extension Sequence {

    func asyncSerialCompactMap<T>(_ transform: @escaping (Element) async throws -> T?) async rethrows -> [T] {
        var values = [T]()

        for element in self {
            if let transformed = try await transform(element) {
                values.append(transformed)
            }
        }

        return values
    }

    func asyncParallelCompactMap<T>(_ transform: @escaping (Element) async throws -> T?) async rethrows -> [T] {
        return try await withThrowingTaskGroup(of: (Int, T?).self) { taskGroup -> [T] in
            for (index, element) in self.enumerated() {
                taskGroup.addTask {
                    let task = try await transform(element)
                    return (index, task)
                }
            }

            var groupResults = [(index: Int, element: T?)]()
            for try await value in taskGroup {
                groupResults.append(value)
            }

            groupResults.sort(by: { $0.index < $1.index })

            return groupResults.compactMap { $0.element }
        }
    }

    func asyncSerialFlatMap<T>(_ transform: @escaping (Element) async throws -> [T]) async rethrows -> [T] {
        var values = [T]()

        for element in self {
            values.append(contentsOf: try await transform(element))
        }

        return values
    }

    func asyncParallelFlatMap<T>(_ transform: @escaping (Element) async throws -> [T]) async rethrows -> [T] {
        return try await withThrowingTaskGroup(of: (Int, [T]).self) { taskGroup -> [T] in
            for (index, element) in self.enumerated() {
                taskGroup.addTask {
                    let task = try await transform(element)
                    return (index, task)
                }
            }

            var groupResults = [(index: Int, element: [T])]()
            for try await value in taskGroup {
                groupResults.append(value)
            }

            groupResults.sort(by: { $0.index < $1.index })

            return groupResults.flatMap({ $0.element })
        }
    }
}
