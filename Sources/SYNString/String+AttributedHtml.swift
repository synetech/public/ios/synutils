//
//  String+AttributedHtml.swift
//  AnonymousAccess
//
//  Created by Tomáš Novotný on 17/06/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if os(iOS)
import SYNFont
import UIKit

public extension String {

    typealias Options = [NSAttributedString.DocumentReadingOptionKey: Any]
    typealias MutableString = NSMutableAttributedString
    
    /// Formats html string to NSAttributedString
    /// where HTML tags in the original string are replaced with corresponding NSAttributes
    /// - Parameter fontPackage: package of fonts that will be used to replace default attributed font
    func getHtmlAttributedString(fontPackage: FontPackage? = nil) -> NSAttributedString {
        let attributedString = getDefaultHtmlFormattedString(from: self)
        guard let mutable = attributedString?.mutableCopy()
            as? MutableString else { return NSAttributedString() }
        guard fontPackage != nil else { return mutable }
        // swiftlint:disable force_unwrapping
        return replaceDefaultFont(string: mutable, fontPackage: fontPackage!)
            ?? NSAttributedString()
    }
}

private extension String {
    
    func getDefaultHtmlFormattedString(from: String) -> NSAttributedString? {
        let options: Options
            = [.documentType: NSAttributedString.DocumentType.html,
               .characterEncoding: String.Encoding.utf8.rawValue]
        guard let data = data(using: .utf8) else { return nil }
        return formatHtmlString(data, options: options)
    }

    func formatHtmlString(_ data: Data, options: Options) -> NSAttributedString? {
        do {
            return try NSAttributedString(data: data,
                                          options: options,
                                          documentAttributes: nil)
        } catch {
            return nil
        }
    }

    func replaceDefaultFont(string: MutableString,
                                    fontPackage: FontPackage) -> NSAttributedString? {
        for index in 0..<string.length {
            replaceDefaultFont(in: string,
                               at: index,
                               fontPackage: fontPackage)
        }
        return string as NSAttributedString
    }

    func replaceDefaultFont(in string: MutableString,
                                    at index: Int,
                                    fontPackage: FontPackage) {
        let currentRange = NSRange(location: index, length: 1)
        let attributes = string.attributes(at: index, effectiveRange: nil)
        guard let attributedFont = attributes[NSAttributedString.Key.font]
            as? UIFont else { return }
        let targetFont = fontPackage.getTargetFont(for: attributedFont)
        string.addAttribute(.font, value: targetFont as Any, range: currentRange)
    }
}
#endif
