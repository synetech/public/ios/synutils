//
//  String+Capitalize+Lowercase.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

public extension String {

    func capitalizeFirstCharacter() -> String {
        #if os(iOS)
        return firstLetter.localizedUppercase + dropFirst()
        #else
        return firstLetter.uppercased() + dropFirst()
        #endif
    }

    func lowercaseFirstCharacter() -> String {
        #if os(iOS)
        return firstLetter.localizedLowercase + dropFirst()
        #else
        return firstLetter.lowercased() + dropFirst()
        #endif
    }
}
