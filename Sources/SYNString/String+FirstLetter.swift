//
//  String+FirstLetter.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

public extension String {

    var firstLetter: String {
        return String(self.prefix(1))
    }
}
