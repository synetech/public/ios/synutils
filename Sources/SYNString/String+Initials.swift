//
//  String+Initials.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

#if os(iOS)
public extension String {

    var initials: String {
        return components(separatedBy: " ")
            .map({ (string) in
                return String(string.firstLetter)
            })
            .reduce("") { (resultString, firstLetter) in
                return resultString.appending(firstLetter)
            }
        }
}
#endif
