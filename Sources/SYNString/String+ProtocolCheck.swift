//
//  String+ProtocolCheck.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

#if os(iOS)
public extension String {
    
    var hasProtocol: Bool {
        return hasPrefix("https://") || hasPrefix("http://")
    }

    var hasSecuredProtocol: Bool {
        return hasPrefix("https://")
    }

    func addSecuredProtocol() -> String {
        let partAfterProtocol = components(separatedBy: "://").last ?? ""
        return "https://" + partAfterProtocol
    }
}
#endif
