//
//  String+StrikeThroughText.swift
//  AnonymousAccess
//
//  Created by Tomáš Novotný on 15/08/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if os(iOS)
import UIKit

public extension String {

    var strikeThroughText: NSMutableAttributedString {
        let attributeString = NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                     value: NSUnderlineStyle.single.rawValue,
                                     range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
}
#endif
