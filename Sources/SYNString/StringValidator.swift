//
//  StringValidator.swift
//  
//
//  Created by Lukáš Růžička on 14/08/2020.
//

import Foundation

public struct StringValidator {

    public typealias InputType = String

    public static func validateNonEmptyString(_ input: InputType) -> Bool {
        return !input.trimmingCharacters(in: .whitespaces).isEmpty
    }

    public static func validateStringLenght(_ input: InputType, validLenghtMin: Int) -> Bool {
        return input.count >= validLenghtMin
    }

    public static func validateStringLenghtMax(_ input: InputType, maxLength: Int) -> Bool {
        return input.count <= maxLength
    }

    /// The password should have three of the 4 types of characters (lowecase letter, uppercase letter, digit, special case letter)
    public static func validatePassword(_ input: InputType) -> Bool {
        let lowercaseUppercaseDigitRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}"
        if NSPredicate(format: "SELF MATCHES %@", lowercaseUppercaseDigitRegex).evaluate(with: input) {
            return true
        }
        let lowercaseUppercaseSpecialRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\W).{8,}"
        if NSPredicate(format: "SELF MATCHES %@", lowercaseUppercaseSpecialRegex).evaluate(with: input) {
            return true
        }
        let lowercaseDigitSpecialRegex = "^(?=.*[a-z])(?=.*[0-9])(?=.*\\W).{8,}"
        if NSPredicate(format: "SELF MATCHES %@", lowercaseDigitSpecialRegex).evaluate(with: input) {
            return true
        }
        let uppercaseDigitSpecialRegex = "^(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).{8,}"
        if NSPredicate(format: "SELF MATCHES %@", uppercaseDigitSpecialRegex).evaluate(with: input) {
            return true
        }
        return false
    }

    public static func validateEmail(_ input: InputType) -> Bool {
        let regEx = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}([-.]\\w+)*$"
        let test = NSPredicate(format: "SELF MATCHES %@", regEx)
        return test.evaluate(with: input)
    }
}
