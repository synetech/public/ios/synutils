//
//  ContentTypeHeader.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

public enum ContentTypeHeader: String {

    case urlEncoded = "application/x-www-form-urlencoded"
    case plain = "text/plain"
    case json = "application/json"

    public static var key: String {
        return "Content-Type"
    }
}
