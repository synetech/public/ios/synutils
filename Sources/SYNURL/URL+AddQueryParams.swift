//
//  URL+AddQueryParams.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import Foundation

public extension URL {

    mutating func addQueryParams(params: [String: String]) {
        guard !params.isEmpty,
              var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: false) else { return }
        urlComponents.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        if let urlWithParams = urlComponents.url {
            self = urlWithParams
        }
    }
}
