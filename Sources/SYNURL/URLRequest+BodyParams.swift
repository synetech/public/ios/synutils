//
//  URLRequest+BodyParams.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import Foundation

public extension URLRequest {

    mutating func addBodyParams(params: [String: Any]) {
        if let contentType = allHTTPHeaderFields?[ContentTypeHeader.key] {
            if contentType.contains(ContentTypeHeader.urlEncoded.rawValue) {
                let rawBody = params.reduce(into: "") { (result, keyValuePair) in
                    // NOTE: All ampersands need to be encoded,
                    // otherwise it would be handled as a parameter delimiter
                    let key = keyValuePair.key.replaceAmpersands()
                    let value = "\(keyValuePair.value)".replaceAmpersands()
                    result += "\(key)=\(value)&"
                }
                httpBody = rawBody.data(using: .utf8)
            } else if contentType.contains(ContentTypeHeader.plain.rawValue)
                        || contentType.contains(ContentTypeHeader.json.rawValue) {
                httpBody = try? JSONSerialization.data(withJSONObject: params)
            }
        }
    }
}

// MARK: - String values adjusting
private extension String {

    func replaceAmpersands() -> String {
        return replacingOccurrences(of: "&", with: "%26")
    }
}
