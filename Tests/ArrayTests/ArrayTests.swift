//
//  ArrayTests.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

import XCTest
@testable import SYNArray

final class ArrayTests: XCTestCase {

    func testElementAtIndex() {
        let array: [Int] = [1,2,3]

        XCTAssertEqual(array.element(at: -1), nil)
        XCTAssertEqual(array.element(at: 0), 1)
        XCTAssertEqual(array.element(at: 1), 2)
        XCTAssertEqual(array.element(at: 2), 3)
        XCTAssertEqual(array.element(at: 3), nil)
        XCTAssertEqual(array.element(at: 325345345), nil)
    }
}
