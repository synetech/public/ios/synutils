//
//  CombineCompletionTests.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import Combine
import XCTest
@testable import SYNCombine

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
final class CombineCompletionTests: XCTestCase {

    // MARK: - Subtypes
    enum TestError: Error {

        case test
    }

    // MARK: - Tests
    func testCompletionIsSuccess() {
        let expectation = XCTestExpectation()
        let _  = Just<Bool>(true)
            .sink(receiveCompletion: { completion in
                XCTAssertTrue(completion.isSuccess)
                XCTAssertFalse(completion.isFailure)
                expectation.fulfill()
            }, receiveValue: { _ in })

        wait(for: [expectation], timeout: 1)
    }

    func testCompletionIsFailure() {
        let expectation = XCTestExpectation()
        let _  = Fail<Bool, TestError>(error: .test)
            .sink(receiveCompletion: { completion in
                XCTAssertFalse(completion.isSuccess)
                XCTAssertTrue(completion.isFailure)
                expectation.fulfill()
            }, receiveValue: { _ in })

        wait(for: [expectation], timeout: 1)
    }
}
