//
//  CombineFirstValueAsyncTests.swift
//  
//
//  Created by Stepan Kloucek on 13.10.2022.
//

import Combine
import XCTest
@testable import SYNCombine

@available(iOS 13.0, *)
final class CombineFirstValueAsyncTests: XCTestCase {

    // MARK: - Subtypes
    enum TestError: Error {

        case test
    }

    // MARK: - Properties

    @Published var publishedVar = 0

    override func setUp() async throws {
        try await super.setUp()
        publishedVar = 0
    }

    // MARK: - Tests
    func testFirstValueAsync() {
        let expectation = XCTestExpectation()
        Task {
            _ = await Just<Bool>(true)
                .eraseToAnyPublisher()
                .firstValueAsync()
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
    }

    func testFirstValueAsyncMultipleValues() {

        let expectation = XCTestExpectation()
        Task {
            let value = await $publishedVar
                .filter { $0 > 0 }
                .eraseToAnyPublisher()
                .firstValueAsync()
            expectation.fulfill()
            XCTAssertTrue(value == 1)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.publishedVar = 1
            self?.publishedVar = 2
            self?.publishedVar = 3
        }

        wait(for: [expectation], timeout: 1)
    }

    func testFirstThrowingValueAsyncValue() {
        let expectation = XCTestExpectation()
        let publisher = PassthroughSubject<Bool, Error>()

        Task {
            _ = try await publisher
                .firstValueAsync()
            expectation.fulfill()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            publisher.send(true)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testFirstThrowingValueAsyncError() {
        let expectation = XCTestExpectation()
        let publisher = PassthroughSubject<Bool, Error>()

        Task {
            do  {
                _ = try await publisher
                    .firstValueAsync()
            } catch {
                expectation.fulfill()
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            publisher.send(completion: .failure(TestError.test))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testFirstValueThrowingAsyncMultipleValues() {
        let expectation = XCTestExpectation()
        let publisher = PassthroughSubject<Int, Error>()

        Task {
            let value = try await publisher
                .filter { $0 > 0 }
                .eraseToAnyPublisher()
                .firstValueAsync()
            expectation.fulfill()
            XCTAssertTrue(value == 1)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            publisher.send(1)
            publisher.send(2)
            publisher.send(3)
        }

        wait(for: [expectation], timeout: 1)
    }
}
