//
//  DateComparingDateComponentTests.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import XCTest
@testable import SYNDate

final class DateComparingDateComponentTests: XCTestCase {

    // MARK: - Tests
    func testSeconds() {
        testComparationsAgo(for: .second, timeMultiplier: 1)
        testComparationsInFuture(for: .second, timeMultiplier: 1)
    }

    func testMinutes() {
        testComparationsAgo(for: .minute, timeMultiplier: .minute)
        testComparationsInFuture(for: .minute, timeMultiplier: .minute)
    }

    func testHours() {
        testComparationsAgo(for: .hour, timeMultiplier: .hour)
        testComparationsInFuture(for: .hour, timeMultiplier: .hour)
    }

    func testDays() {
        testComparationsAgo(for: .day, timeMultiplier: .day)
        testComparationsInFuture(for: .day, timeMultiplier: .day)
    }

    func testMonths() {
        testComparationsAgo(for: .month, timeMultiplier: .day * 31)
        testComparationsInFuture(for: .month, timeMultiplier: .day * 31)
    }

    func testYears() {
        testComparationsAgo(for: .year, timeMultiplier: .day * 366)
        testComparationsInFuture(for: .year, timeMultiplier: .day * 366)
    }
}

// MARK: - Supporting functions
extension DateComparingDateComponentTests {

    func testComparationsAgo(for component: Date.KnownCalendarComponent, timeMultiplier: TimeInterval) {
        let fiveAgo = Date().addingTimeInterval(-timeMultiplier * 5)
        XCTAssertTrue(fiveAgo.isLess(than: 10, component, .ago))
        XCTAssertFalse(fiveAgo.isLess(than: 3, component, .ago))
        XCTAssertTrue(fiveAgo.isLessOrEqual(than: 5, component, .ago))
        XCTAssertFalse(fiveAgo.isLessOrEqual(than: 4, component, .ago))
        XCTAssertTrue(fiveAgo.isMore(than: 3, component, .ago))
        XCTAssertFalse(fiveAgo.isMore(than: 10, component, .ago))
        XCTAssertTrue(fiveAgo.isMoreOrEqual(than: 5, component, .ago))
        XCTAssertFalse(fiveAgo.isMoreOrEqual(than: 6, component, .ago))
    }

    func testComparationsInFuture(for component: Date.KnownCalendarComponent, timeMultiplier: TimeInterval) {
        let fiveInFuture = Date().addingTimeInterval(timeMultiplier * 5.5)
        XCTAssertTrue(fiveInFuture.isLess(than: 10, component, .inFuture))
        XCTAssertFalse(fiveInFuture.isLess(than: 3, component, .inFuture))
        XCTAssertTrue(fiveInFuture.isLessOrEqual(than: 5, component, .inFuture))
        XCTAssertFalse(fiveInFuture.isLessOrEqual(than: 4, component, .inFuture))
        XCTAssertTrue(fiveInFuture.isMore(than: 3, component, .inFuture))
        XCTAssertFalse(fiveInFuture.isMore(than: 10, component, .inFuture))
        XCTAssertTrue(fiveInFuture.isMoreOrEqual(than: 5, component, .inFuture))
        XCTAssertFalse(fiveInFuture.isMoreOrEqual(than: 6, component, .inFuture))
    }
}
