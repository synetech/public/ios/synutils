//
//  DateComparingTests.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

import Foundation
import XCTest
@testable import SYNDate

final class DateComparingOtherDateTests: XCTestCase {

    // MARK: - Properties
    private let currentDate = Date()
    private lazy var tenMinutesInPast = Calendar.current.date(byAdding: .minute, value: -10, to: currentDate)!

    // MARK: - Tests
    func testLessThan() {
        XCTAssertTrue(currentDate.isLessThan(20, unit: .minute, from: tenMinutesInPast))
        XCTAssertFalse(currentDate.isLessThan(10, unit: .minute, from: tenMinutesInPast))
        XCTAssertFalse(currentDate.isLessThan(5, unit: .minute, from: tenMinutesInPast))
    }

    func testLessThanOrEqual() {
        XCTAssertTrue(currentDate.isLessThanOrEqual(20, unit: .minute, from: tenMinutesInPast))
        XCTAssertTrue(currentDate.isLessThanOrEqual(10, unit: .minute, from: tenMinutesInPast))
        XCTAssertFalse(currentDate.isLessThanOrEqual(5, unit: .minute, from: tenMinutesInPast))
    }

    func testGreaterThan() {
        XCTAssertFalse(currentDate.isGreaterThan(20, unit: .minute, from: tenMinutesInPast))
        XCTAssertFalse(currentDate.isGreaterThan(10, unit: .minute, from: tenMinutesInPast))
        XCTAssertTrue(currentDate.isGreaterThan(5, unit: .minute, from: tenMinutesInPast))
    }

    func testGreaterThanOrEqual() {
        XCTAssertFalse(currentDate.isGreaterThanOrEqual(20, unit: .minute, from: tenMinutesInPast))
        XCTAssertTrue(currentDate.isGreaterThanOrEqual(10, unit: .minute, from: tenMinutesInPast))
        XCTAssertTrue(currentDate.isGreaterThanOrEqual(5, unit: .minute, from: tenMinutesInPast))
    }
    
    func testIsToday() {
        XCTAssertTrue(currentDate.isToday)
        
        let dayInPast = Calendar.current.date(byAdding: .day, value: -1, to: currentDate)!
        XCTAssertFalse(dayInPast.isToday)
        let dayInFuture = Calendar.current.date(byAdding: .day, value: 1, to: currentDate)!
        XCTAssertFalse(dayInFuture.isToday)
    }
}
