//
//  DateFormatTest.swift
//  
//
//  Created by Štěpán Klouček on 15.03.2021.
//

import Foundation
import XCTest
@testable import SYNDate

final class DateFormatTest: XCTestCase {

    // MARK: - Properties
    private let date1970 = Date.init(timeIntervalSince1970: 0)

    // MARK: - Tests
    func testdayOfMonth() {
        
        XCTAssertEqual(date1970.dayOfMonth, "01")
    }
    
    func testDayTime() {
        XCTAssertEqual(date1970.dayTime, "01:00")
    }
    
    func testDateString() {
        XCTAssertEqual(date1970.dateString, "01. 01. 1970")
    }
    
    func testDateAndTime() {
        XCTAssertEqual(date1970.dateAndTime, "01. 01. 1970, 01:00")
    }
}
