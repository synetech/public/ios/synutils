//
//  GeohashRegionTests.swift
//  
//
//  Created by Lukáš Růžička on 27/08/2020.
//

import XCTest
@testable import SYNGeohash

final class GeohashRegionTests: XCTestCase {

    // MARK: - Tests
    func testCommonGeohashRegion5x4() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: 50.009765625, maxLatitude: 50.537109375,
                                                          minLongitude: 13.88671875, maxLongitude: 15.29296875,
                                                          precision: 4)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "u2cz", topRight: "u2fz", bottomLeft: "u2cu")

        let expectedResult = ["u2cz", "u2cy", "u2cv", "u2cu",
                              "u2fp", "u2fn", "u2fj", "u2fh",
                              "u2fr", "u2fq", "u2fm", "u2fk",
                              "u2fx", "u2fw", "u2ft", "u2fs",
                              "u2fz", "u2fy", "u2fv", "u2fu"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testCommonGeohashRegion2x2() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: 49.833984375, maxLatitude: 50.009765625,
                                                          minLongitude: 17.05078125, maxLongitude: 17.40234375,
                                                          precision: 4)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "u2uh", topRight: "u2uk", bottomLeft: "u2u5")

        let expectedResult = ["u2uh", "u2u5",
                              "u2uk", "u2u7"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testCommonGeohashRegion2x1() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: 49.833984375, maxLatitude: 50.009765625,
                                                          minLongitude: 17.05078125, maxLongitude: 17.05078125,
                                                          precision: 4)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "u2uh", topRight: "u2uh", bottomLeft: "u2u5")

        let expectedResult = ["u2uh", "u2u5"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testCommonGeohashRegion1x1() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: 50.009765625, maxLatitude: 50.009765625,
                                                          minLongitude: 17.05078125, maxLongitude: 17.05078125,
                                                          precision: 4)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "u2uh", topRight: "u2uh", bottomLeft: "u2uh")

        let expectedResult = ["u2uh"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    /// Somewhere in Pacific ocean
    func testDateLineGeohashRegion4x3() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: 42.978515625, maxLatitude: 43.330078125,
                                                          minLongitude: 179.47265625, maxLongitude: -179.47265625,
                                                          precision: 4)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "xzxw", topRight: "8p8q", bottomLeft: "xzxs")

        let expectedResult = ["xzxw", "xzxt", "xzxs",
                              "xzxy", "xzxv", "xzxu",
                              "8p8n", "8p8j", "8p8h",
                              "8p8q", "8p8m", "8p8k"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testGreenwichGeohashRegion2x3() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: 51.240234375, maxLatitude: 51.591796875,
                                                          minLongitude: -0.17578125, maxLongitude: 0.17578125,
                                                          precision: 4)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "gcpv", topRight: "u10j", bottomLeft: "gcpg")

        let expectedResult = ["gcpv", "gcpu", "gcpg",
                              "u10j", "u10h", "u105"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testPrecision1GeohashRegion4x4() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: -67.5, maxLatitude: 67.5,
                                                          minLongitude: -67.5, maxLongitude: 67.5,
                                                          precision: 1)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "f", topRight: "v", bottomLeft: "4")

        let expectedResult = ["f", "d", "6", "4",
                              "g", "e", "7", "5",
                              "u", "s", "k", "h",
                              "v", "t", "m", "j"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testPrecision2GeohashRegion3x3() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: -8.4375, maxLatitude: 2.8125,
                                                          minLongitude: 84.375, maxLongitude: 106.875,
                                                          precision: 2)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "tb", topRight: "w2", bottomLeft: "my")

        let expectedResult = ["tb", "mz", "my",
                              "w0", "qp", "qn",
                              "w2", "qr", "qq"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testPrecision3GeohashRegion3x3() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: 54.140625, maxLatitude: 56.953125,
                                                          minLongitude: -103.359375, maxLongitude: -100.546875,
                                                          precision: 3)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "cdn", topRight: "cf0", bottomLeft: "c9w")

        let expectedResult = ["cdn", "c9y", "c9w",
                              "cdp", "c9z", "c9x",
                              "cf0", "ccb", "cc8"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }

    func testPrecision5GeohashRegion3x3() {
        let geohashRegionCoordinates = try? GeohashRegion(minLatitude: -37.85888671875, maxLatitude: -37.77099609375,
                                                          minLongitude: 144.86572265625, maxLongitude: 144.95361328125,
                                                          precision: 5)
        XCTAssertNotNil(geohashRegionCoordinates)
        let geohashRegionCorners = GeohashRegion(topLeft: "r1r10", topRight: "r1r14", bottomLeft: "r1r08")

        let expectedResult = ["r1r10", "r1r0b", "r1r08",
                              "r1r11", "r1r0c", "r1r09",
                              "r1r14", "r1r0f", "r1r0d"]

        checkGeohashes(in: geohashRegionCoordinates!, expectedResult: expectedResult)
        checkGeohashes(in: geohashRegionCorners, expectedResult: expectedResult)
    }
}

// MARK: - Supporting functions
private extension GeohashRegionTests {

    func checkGeohashes(in geohashRegion: GeohashRegion, expectedResult: [String]) {
        let result = geohashRegion.getAllGeohashes()

        XCTAssertEqual(result.count, expectedResult.count)
        result.forEach { XCTAssertTrue(expectedResult.contains($0), "Geohash \($0) is missing") }
    }
}
