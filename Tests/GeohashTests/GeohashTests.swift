//
//  GeohashTests.swift
//  
//
//  Created by Lukáš Růžička on 28/07/2020.
//

import XCTest
@testable import SYNGeohash

final class GeohashTests: XCTestCase {

    func testEncoding() {
        XCTAssertEqual(Geohash.encode(latitude: 42.6, longitude: -5.6), "ezs42")
        XCTAssertEqual(Geohash.encode(latitude: -25.382708, longitude: -49.265506, 12), "6gkzwgjzn820")
    }

    func testDecoding() {
        XCTAssertEqual(Geohash.decode("ezs42")?.latitude, 42.60498046875)
        XCTAssertEqual(Geohash.decode("ezs42")?.longitude, -5.60302734375)
    }

    func testNeighbours() {
        XCTAssertEqual(Geohash.neighbour("u000", direction: .north), "u001")
        XCTAssertEqual(Geohash.neighbour("u000", direction: .south), "spbp")
        XCTAssertEqual(Geohash.neighbour("u000", direction: .east), "u002")
        XCTAssertEqual(Geohash.neighbour("u000", direction: .west), "gbpb")
    }
}
