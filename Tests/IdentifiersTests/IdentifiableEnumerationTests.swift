//
//  IdentifiableEnumerationTests.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

import XCTest
@testable import SYNIdentifiers

final class IdentifiableEnumerationTests: XCTestCase {

    func testIdentifiableEnumeration() {
        XCTAssertEqual(Homepage.title.identifier, "homepage")
        XCTAssertEqual(Homepage.title.rawValue, "homepageTitle")

        XCTAssertEqual(Homepage.menu.identifier, "homepage")
        XCTAssertEqual(Homepage.menu.rawValue, "homepageMenu")
    }

    func testNestedIdentifiableEnumeration() {
        XCTAssertEqual(Homepage.Section.icon.superTypeIdentifier, "homepage")
        XCTAssertEqual(Homepage.Section.icon.identifier, "homepageSection")
        XCTAssertEqual(Homepage.Section.icon.rawValue, "homepageSectionIcon")

        XCTAssertEqual(Homepage.Section.description.superTypeIdentifier, "homepage")
        XCTAssertEqual(Homepage.Section.description.identifier, "homepageSection")
        XCTAssertEqual(Homepage.Section.description.rawValue, "homepageSectionDescription")
    }
}

private enum Homepage: String, IdentifiableEnumeration {

    case title
    case menu

    enum Section: String, NestedIdentifiableEnumeration {

        typealias SuperType = Homepage

        case icon
        case description
    }
}
