//
//  IdentifiableErrorTests.swift
//  
//
//  Created by Lukáš Růžička on 27/08/2020.
//

import XCTest
@testable import SYNIdentifiers

final class IdentifiableErrorTests: XCTestCase {

    func testIdentifiableError() {
        XCTAssertEqual(NetworkingError.lostConnection.errorDescription, "networkingErrorLostConnection")

        XCTAssertEqual(NetworkingError.serverError.errorDescription, "networkingErrorServerError")
    }
}

private enum NetworkingError: IdentifiableError {

    case lostConnection
    case serverError
}
