//
//  ComparableClampedToTests.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import XCTest
@testable import SYNNumbers

final class ComparableClampedToTests: XCTestCase {

    // MARK: - Tests
    func testInt() {
        let range: ClosedRange<Int> = 0...10
        XCTAssertEqual(Int(5).clamped(to: range), 5)
        XCTAssertEqual(Int(0).clamped(to: range), 0)
        XCTAssertEqual(Int(-1).clamped(to: range), 0)
        XCTAssertEqual(Int(-11).clamped(to: range), 0)
        XCTAssertEqual(Int(10).clamped(to: range), 10)
        XCTAssertEqual(Int(11).clamped(to: range), 10)
        XCTAssertEqual(Int(21).clamped(to: range), 10)
    }

    func testFloat() {
        let range: ClosedRange<Float> = 0...10
        XCTAssertEqual(Float(5).clamped(to: range), 5)
        XCTAssertEqual(Float(0).clamped(to: range), 0)
        XCTAssertEqual(Float(-0.1).clamped(to: range), 0)
        XCTAssertEqual(Float(-10.1).clamped(to: range), 0)
        XCTAssertEqual(Float(10).clamped(to: range), 10)
        XCTAssertEqual(Float(10.1).clamped(to: range), 10)
        XCTAssertEqual(Float(20.1).clamped(to: range), 10)
    }

    func testCGFloat() {
        let range: ClosedRange<CGFloat> = 0...10
        XCTAssertEqual(CGFloat(5).clamped(to: range), 5)
        XCTAssertEqual(CGFloat(0).clamped(to: range), 0)
        XCTAssertEqual(CGFloat(-0.1).clamped(to: range), 0)
        XCTAssertEqual(CGFloat(-10.1).clamped(to: range), 0)
        XCTAssertEqual(CGFloat(10).clamped(to: range), 10)
        XCTAssertEqual(CGFloat(10.1).clamped(to: range), 10)
        XCTAssertEqual(CGFloat(20.1).clamped(to: range), 10)
    }

    func testDouble() {
        let range: ClosedRange<Double> = 0...10
        XCTAssertEqual(Double(5).clamped(to: range), 5)
        XCTAssertEqual(Double(0).clamped(to: range), 0)
        XCTAssertEqual(Double(-0.1).clamped(to: range), 0)
        XCTAssertEqual(Double(-10.1).clamped(to: range), 0)
        XCTAssertEqual(Double(10).clamped(to: range), 10)
        XCTAssertEqual(Double(10.1).clamped(to: range), 10)
        XCTAssertEqual(Double(20.1).clamped(to: range), 10)
    }
}
