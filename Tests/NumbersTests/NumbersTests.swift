//
//  NumbersTests.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

import Foundation
import XCTest
@testable import SYNNumbers

final class NumbersTests: XCTestCase {

    // MARK: - Tests
    func testFormattingDecimals() {
        // Double
        XCTAssertEqual(Double.pi.roundedString(to: 0), "3")
        XCTAssertEqual(Double.pi.roundedString(to: 2), "3.14")
        XCTAssertEqual(Double.pi.roundedString(to: 6), "3.141593")

        // Float
        XCTAssertEqual(Float.pi.roundedString(to: 0), "3")
        XCTAssertEqual(Float.pi.roundedString(to: 2), "3.14")
        XCTAssertEqual(Float.pi.roundedString(to: 6), "3.141593")
    }
}
