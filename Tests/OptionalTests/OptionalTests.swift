//
//  OptionalTests.swift
//  
//
//  Created by Lukáš Růžička on 28/10/2019.
//

import XCTest
@testable import SYNOptional

final class OptionalTests: XCTestCase {

    func testUnwrapping() {
        let string: String? = nil
        XCTAssertEqual(string.unwrap(), "")

        let character: Character? = nil
        XCTAssertEqual(character.unwrap(), " ")

        let int: Int? = nil
        XCTAssertEqual(int.unwrap(), 0)

        let double: Double? = nil
        XCTAssertEqual(double.unwrap(), 0.0)

        let float: Float? = nil
        XCTAssertEqual(float.unwrap(), 0.0)

        let cgfloat: CGFloat? = nil
        XCTAssertEqual(cgfloat.unwrap(), 0.0)

        let bool: Bool? = nil
        XCTAssertTrue(bool.trueUnwrap())
        XCTAssertFalse(bool.falseUnwrap())
    }
}
