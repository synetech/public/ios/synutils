//
//  SequenceTests.swift
//  SequenceTests
//
//  Created by Matěj on 02.11.2022.
//

import XCTest
@testable import SYNSequence

final class AsyncCompactMapTests: XCTestCase {

    private var filledArray: [Int?] = [0, nil, 2, 3, nil, 5]
    private var emptyArray: [Int?] = []

    func testCompactFunction() async throws {
        let compactArray = filledArray.compactMap { $0 }
        let asyncSerialCompactArray = await filledArray.asyncCompactMap { $0 }
        let asyncParallelCompactArray = await filledArray.asyncCompactMap(parallel: true) { $0 }

        XCTAssertEqual(compactArray, asyncSerialCompactArray)
        XCTAssertEqual(asyncSerialCompactArray, asyncParallelCompactArray)
    }

    func testMapFunction() async throws {
        let compactArray = filledArray.compactMap { intToString($0) }
        let asyncSerialCompactArray = await filledArray.asyncCompactMap { self.intToString($0) }
        let asyncParallelCompactArray = await filledArray.asyncCompactMap(parallel: true) { self.intToString($0) }

        XCTAssertEqual(compactArray, asyncSerialCompactArray)
        XCTAssertEqual(asyncSerialCompactArray, asyncParallelCompactArray)
    }

    func testEmptyArray() async throws {
        let compactArray = emptyArray.compactMap { $0 }
        let asyncSerialCompactArray = await emptyArray.asyncCompactMap { $0 }
        let asyncParallelCompactArray = await emptyArray.asyncCompactMap(parallel: true) { $0 }

        XCTAssertEqual(compactArray, asyncSerialCompactArray)
        XCTAssertEqual(asyncSerialCompactArray, asyncParallelCompactArray)
    }
}

// MARK: Supporting functions
extension AsyncCompactMapTests {

    func intToString(_ number: Int?) -> String? {
        if let number = number {
            return "\(number)"
        } else {
            return nil
        }
    }
}
