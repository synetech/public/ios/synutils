//
//  AsyncFlatMapTests.swift
//  SequenceTests
//
//  Created by Matěj on 03.11.2022.
//

import XCTest
@testable import SYNSequence

final class AsyncFlatMapTests: XCTestCase {

    private let filledArray: [[Int]] = [[0, 1], [2, 3]]
    private let emptyArray: [[Int]] = []

    func testFlatFunction() async throws {
        let flatArray = filledArray.flatMap { $0 }
        let asyncSerialFlatArray = await filledArray.asyncFlatMap { $0 }
        let asyncParallelFlatArray = await filledArray.asyncFlatMap(parallel: true) { $0 }

        XCTAssertEqual(flatArray, asyncSerialFlatArray)
        XCTAssertEqual(asyncSerialFlatArray, asyncParallelFlatArray)
    }

    func testMapFunction() async throws {
        let flatArray = filledArray.flatMap { intToString($0) }
        let asyncSerialFlatArray = await filledArray.asyncFlatMap { self.intToString($0) }
        let asyncParallelFlatArray = await filledArray.asyncFlatMap(parallel: true) { self.intToString($0) }

        XCTAssertEqual(flatArray, asyncSerialFlatArray)
        XCTAssertEqual(asyncSerialFlatArray, asyncParallelFlatArray)
    }

    func testEmptyArray() async throws {
        let flatArray = emptyArray.flatMap { $0 }
        let asyncSerialFlatArray = await emptyArray.asyncFlatMap { $0 }
        let asyncParallelFlatArray = await emptyArray.asyncFlatMap(parallel: true) { $0 }

        XCTAssertEqual(flatArray, asyncSerialFlatArray)
        XCTAssertEqual(asyncSerialFlatArray, asyncParallelFlatArray)
    }
}

// MARK: Supporting functions
extension AsyncFlatMapTests {

    func intToString(_ numbers: [Int]) -> [String] {
        numbers.map { "\($0)" }
    }
}


