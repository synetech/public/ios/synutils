//
//  SequenceTests.swift
//  GeohashTests
//
//  Created by Matěj on 28.10.2022.
//

import XCTest
@testable import SYNSequence

final class AsyncMapSpeedTests: XCTestCase {

    private let array: [Int] = [0, 1, 2, 3, 4]

    // Test should run approximatly 0.01 sec
    func testAsyncCompactMapParallelism() async throws {
        measure {
            let exp = expectation(description: "Finished")
            Task {
                _ = await array.asyncCompactMap(parallel: true) { number in
                    await self.wait()
                }
                exp.fulfill()
            }
            wait(for: [exp], timeout: 1)
        }

    }

    // Test should run approximatly 0.01 sec
    func testAsyncFlatMapParallelism() async throws {
        measure {
            let exp = expectation(description: "Finished")
            Task {
                _ = await array.asyncFlatMap(parallel: true) { number in
                    await self.waitAndReturnIdentity(int: number)
                }
                exp.fulfill()
            }
            wait(for: [exp], timeout: 1)
        }

    }

    // Test should run approximatly 0.05 sec
    func testAsyncCompactMapSerialism() async throws {
        measure {
            let exp = expectation(description: "Finished")
            Task {
                _ = await array.asyncCompactMap(parallel: false) { _ in
                    await self.wait()
                }
                exp.fulfill()
            }
            wait(for: [exp], timeout: 1)
        }
    }

    // Test should run approximatly 0.05 sec
    func testAsyncFlatMapSerialism() async throws {
        measure {
            let exp = expectation(description: "Finished")
            Task {
                _ = await array.asyncFlatMap(parallel: false) { number in
                    await self.waitAndReturnIdentity(int: number)
                }
                exp.fulfill()
            }
            wait(for: [exp], timeout: 1)
        }

    }

}


// MARK: Supporting functions
private extension AsyncMapSpeedTests {

    func wait() async {
        Thread.sleep(forTimeInterval: 0.01)
    }

    func waitAndReturnIdentity(int: Int) async -> [Int] {
        Thread.sleep(forTimeInterval: 0.01)
        return [int]
    }
}
