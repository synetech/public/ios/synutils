//
//  String+AttributedHtmlTests.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

import XCTest

@testable import SYNFont
@testable import SYNString

final class StringAttributedHtmlTests: XCTestCase {
    
    #if os(iOS)
    func testFormatting_WithoutFontPackage() {
        let testHtmlString = "<HTML>text</HTML>"
        let formatted = testHtmlString.getHtmlAttributedString()
        
        XCTAssertEqual(formatted.string, "text")
        let firstLetterFont = formatted.attribute(.font, at: 0, effectiveRange: nil) as! UIFont
        XCTAssertEqual(firstLetterFont.familyName, "Times New Roman")
    }

    func testFormatting_WithFontPackage() {
        let testHtmlString = "<HTML>text<b>text<</b><i>text</i></HTML>"
        let testFontPackage = FontPackage(regularFont: UIFont.systemFont(ofSize: 14),
                                          boldFont: UIFont.boldSystemFont(ofSize: 14),
                                          italicFont: UIFont.italicSystemFont(ofSize: 14))
        let formatted = testHtmlString.getHtmlAttributedString(fontPackage: testFontPackage)
        
        let firstTextFont = formatted.attribute(.font, at: 0, effectiveRange: nil) as! UIFont
        let secondTextFont = formatted.attribute(.font, at: 5, effectiveRange: nil) as! UIFont
        let thirdTextFont = formatted.attribute(.font, at: 9, effectiveRange: nil) as! UIFont
        
        XCTAssertEqual(firstTextFont.fontName, ".SFUI-Regular")
        XCTAssertEqual(secondTextFont.fontName, ".SFUI-Semibold")
        XCTAssertEqual(thirdTextFont.fontName, ".SFUI-RegularItalic")
    }
    #endif
}
