//
//  String+CapitalizeLower.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

import XCTest

@testable import SYNString

class StringCapitalizeLower: XCTestCase {

    #if os(iOS)
    func testCapitalizeFirstLetter() {
        let testString = "first letter is lowercased"
        let result = testString.capitalizeFirstCharacter()

        XCTAssertEqual(result.firstLetter, "F")
    }

    func testLowercaseFirstLetter() {
        let testString = "First letter is uppercased"
        let result = testString.lowercaseFirstCharacter()

        XCTAssertEqual(result.firstLetter, "f")
    }
    #endif
}
