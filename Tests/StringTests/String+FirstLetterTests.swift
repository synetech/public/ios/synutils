//
//  String+FirstLetterTests.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

import XCTest

@testable import SYNString

class StringFirstLetterTests: XCTestCase {

    func testFIrstLetter() {
        let testString = "First letter is F"
        let firstLetter = testString.firstLetter

        XCTAssertEqual(firstLetter, "F")
    }
}
