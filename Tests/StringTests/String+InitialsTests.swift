//
//  String+InitialsTests.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

import XCTest

@testable import SYNString

class StringInitialsTests: XCTestCase {

    #if os(iOS)
    func testInitials() {
        let testString = "Random Words Here"
        let initials = testString.initials

        XCTAssertEqual(initials, "RWH")
    }
    #endif
}
