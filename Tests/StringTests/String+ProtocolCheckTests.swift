//
//  File.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

import XCTest

@testable import SYNString

class StringProtocolCheckTests: XCTestCase {

    #if os(iOS)
    let testString_WithoutProtocol = "www.synetech.cz"
    let testString_WithNonSecuredProtocol = "http://www.synetech.cz"
    let testString_WithSecuredProtocol = "https://www.synetech.cz"

    func testProtocolCheck() {
        XCTAssertEqual(testString_WithoutProtocol.hasProtocol, false)
        XCTAssertEqual(testString_WithNonSecuredProtocol.hasProtocol, true)
        XCTAssertEqual(testString_WithSecuredProtocol.hasProtocol, true)
    }
        
    func testSecuredProtocolCheck() {
        XCTAssertEqual(testString_WithoutProtocol.hasSecuredProtocol, false)
        XCTAssertEqual(testString_WithNonSecuredProtocol.hasSecuredProtocol, false)
        XCTAssertEqual(testString_WithSecuredProtocol.hasSecuredProtocol, true)
    }

    func testGetWithProtocol() {
        let originallyWithoutProtocol_withAddedProtocol
            = testString_WithoutProtocol.addSecuredProtocol()
        let originallyWithNonSecuredProtocol_withAddedProtocol
            = testString_WithNonSecuredProtocol.addSecuredProtocol()
        
        XCTAssertEqual(originallyWithoutProtocol_withAddedProtocol, testString_WithSecuredProtocol)
        XCTAssertEqual(originallyWithNonSecuredProtocol_withAddedProtocol, testString_WithSecuredProtocol)
    }
    #endif
}
