//
//  File.swift
//  
//
//  Created by Tomáš Novotný on 28/10/2019.
//

import XCTest

@testable import SYNString

final class StringStrikeThroughTextTests: XCTestCase {

    #if os(iOS)
    func testFormatting_WithoutFontPackage() {
        let testString = "text"
        let formatted_WithStrikeThrough = testString.strikeThroughText
        let formatted_WithouthStrikeShrough = NSAttributedString(string: testString)

        let strikeThroughStyle_InStrikeThroughFormattedString = formatted_WithStrikeThrough
            .attribute(.strikethroughStyle, at: 0, effectiveRange: nil) as! Int
        let strikeThroughStyle_InNonStrikeThroughFormattedString = formatted_WithouthStrikeShrough
            .attribute(.strikethroughStyle, at: 0, effectiveRange: nil)
        
        XCTAssertEqual(strikeThroughStyle_InStrikeThroughFormattedString, 1)
        XCTAssertNil(strikeThroughStyle_InNonStrikeThroughFormattedString)
    }
    #endif
}
