//
//  StringValidatorTests.swift
//  
//
//  Created by Lukáš Růžička on 14/08/2020.
//

@testable import SYNString
import XCTest

final class StringValidatorTests: XCTestCase {

    // MARK: - Tests
    func testStringEmptinessValidation() {
        let emptyString = ""
        let nonEmptyString = "some text"

        XCTAssertFalse(StringValidator.validateNonEmptyString(emptyString))
        XCTAssertTrue(StringValidator.validateNonEmptyString(nonEmptyString))
    }

    func testStringLengthValidation() {
        let stringWithLeghtOf5 = "12345"

        XCTAssertTrue(StringValidator.validateStringLenght(stringWithLeghtOf5, validLenghtMin: 4))
        XCTAssertTrue(StringValidator.validateStringLenght(stringWithLeghtOf5, validLenghtMin: 5))
        XCTAssertFalse(StringValidator.validateStringLenght(stringWithLeghtOf5, validLenghtMin: 6))

        XCTAssertFalse(StringValidator.validateStringLenghtMax(stringWithLeghtOf5, maxLength: 4))
        XCTAssertTrue(StringValidator.validateStringLenghtMax(stringWithLeghtOf5, maxLength: 5))
        XCTAssertTrue(StringValidator.validateStringLenghtMax(stringWithLeghtOf5, maxLength: 6))
    }

    func testPasswordValidation() {
        let weakPassword = "slabeheslo"
        let littleWeakPassword1 = "SlabsiHeslo"
        let littleWeakPassword2 = "slabsiheslo123"
        let littleWeakPassword3 = "slabsiheslo!!!"
        let littleWeakPassword4 = "slabsiheslo##"
        let shortPassword = "Kratky1"
        let strongPassword1 = "SilneHeslo69"
        let strongPassword2 = "silneheslo1!"
        let strongPassword3 = "#SilneHeslo"
        let strongPassword4 = "#SILNEHESLO1"

        XCTAssertFalse(StringValidator.validatePassword(weakPassword))
        XCTAssertFalse(StringValidator.validatePassword(littleWeakPassword1))
        XCTAssertFalse(StringValidator.validatePassword(littleWeakPassword2))
        XCTAssertFalse(StringValidator.validatePassword(littleWeakPassword3))
        XCTAssertFalse(StringValidator.validatePassword(littleWeakPassword4))
        XCTAssertFalse(StringValidator.validatePassword(shortPassword))
        XCTAssertTrue(StringValidator.validatePassword(strongPassword1))
        XCTAssertTrue(StringValidator.validatePassword(strongPassword2))
        XCTAssertTrue(StringValidator.validatePassword(strongPassword3))
        XCTAssertTrue(StringValidator.validatePassword(strongPassword4))
    }

    func testEmailValidationInvalid() {
        // Invalid general
        let notEmail = "tohleneniemail"
        let emailWithTwoAtSigns = "email@@email.email"

        XCTAssertFalse(StringValidator.validateEmail(notEmail))
        XCTAssertFalse(StringValidator.validateEmail(emailWithTwoAtSigns))

        // Invalid prefix
        let emailWithDashOnLastPrefixPlace = "email-@email.email"
        let emailWithTwoDotsInRowInPrefix = "email..email@email.email"
        let emailWithDotInFirstPlace = ".email@email.email"
        let emailWithNumberSignInPrefix = "email#email@email.email"
        let emailWithSpaceInPrefix = "e mail@email.email"

        XCTAssertFalse(StringValidator.validateEmail(emailWithDashOnLastPrefixPlace))
        XCTAssertFalse(StringValidator.validateEmail(emailWithTwoDotsInRowInPrefix))
        XCTAssertFalse(StringValidator.validateEmail(emailWithDotInFirstPlace))
        XCTAssertFalse(StringValidator.validateEmail(emailWithNumberSignInPrefix))
        XCTAssertFalse(StringValidator.validateEmail(emailWithSpaceInPrefix))

        // Invalid suffix
        let emailWithoutDomain = "email@email"
        let emailWithDomainWithLengthOfOne = "email@email.e"
        let emailWithNumberSignInSuffix = "email@email#email.email"
        let emailWithTwoDotsInRowInSuffix = "email@email..email"
        let emailWithSpaceInSuffix = "email@e mail.email"

        XCTAssertFalse(StringValidator.validateEmail(emailWithoutDomain))
        XCTAssertFalse(StringValidator.validateEmail(emailWithDomainWithLengthOfOne))
        XCTAssertFalse(StringValidator.validateEmail(emailWithNumberSignInSuffix))
        XCTAssertFalse(StringValidator.validateEmail(emailWithTwoDotsInRowInSuffix))
        XCTAssertFalse(StringValidator.validateEmail(emailWithSpaceInSuffix))
    }

    func testEmailValidationValid() {
        // Valid general
        let validEmail = "email@email.email"
        let validEmailCz = "email@email.cz"
        let validEmailCom = "email@email.com"

        XCTAssertTrue(StringValidator.validateEmail(validEmail))
        XCTAssertTrue(StringValidator.validateEmail(validEmailCz))
        XCTAssertTrue(StringValidator.validateEmail(validEmailCom))

        // Valid special prefix
        let emailWithDotInPrefix = "email.email@email.email"
        let emailWithDashInPrefix = "email-email@email.email"
        let emailWithUnderlineInPrefix = "email_email@email.email"
        let emailWithNumberInPrefix = "email123@email.email"

        XCTAssertTrue(StringValidator.validateEmail(emailWithDotInPrefix))
        XCTAssertTrue(StringValidator.validateEmail(emailWithDashInPrefix))
        XCTAssertTrue(StringValidator.validateEmail(emailWithUnderlineInPrefix))
        XCTAssertTrue(StringValidator.validateEmail(emailWithNumberInPrefix))

        // Valid special suffix
        let emailWithDashInSuffix = "email@email-email.email"
        let emailWithNumberInSuffix = "email@email123.email"
        let emailWithTwoDomains = "email@email.email.email"

        XCTAssertTrue(StringValidator.validateEmail(emailWithDashInSuffix))
        XCTAssertTrue(StringValidator.validateEmail(emailWithNumberInSuffix))
        XCTAssertTrue(StringValidator.validateEmail(emailWithTwoDomains))
    }
}
