//
//  URLQueryParamsTests.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import XCTest
@testable import SYNURL

final class URLQueryParamsTests: XCTestCase {

    // MARK: - Properties
    private let url = URL(string: "https://synetech.cz/endpoint")!

    // MARK: - Tests
    func testAddOneParam() {
        var urlWithParam = url
        urlWithParam.addQueryParams(params: ["key": "value"])

        XCTAssertEqual(urlWithParam.absoluteString, url.absoluteString + "?key=value")
    }

    func testAddTwoParams() {
        var urlWithParams = url
        urlWithParams.addQueryParams(params: ["key1": "value1", "key2": "value2"])

        let queryParamsOrder1 = "?key1=value1&key2=value2"
        let queryParamsOrder2 = "?key2=value2&key1=value1"
        guard urlWithParams.absoluteString == url.absoluteString + queryParamsOrder1
                || urlWithParams.absoluteString == url.absoluteString + queryParamsOrder2 else {
            XCTFail("URL doesn't have the given query params in any order: \(url.absoluteString)")
            return
        }
    }
}
