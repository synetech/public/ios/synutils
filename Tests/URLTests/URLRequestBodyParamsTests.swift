//
//  URLRequestBodyParamsTests.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

import XCTest
@testable import SYNURL

final class URLRequestBodyParamsTests: XCTestCase {

    // MARK: - Properties
    private var request = URLRequest(url: URL(string: "https://synetech.cz/endpoint")!)

    // MARK: - Tests
    func testURLEncodedOneParam() {
        request.allHTTPHeaderFields = [ContentTypeHeader.key: ContentTypeHeader.urlEncoded.rawValue]

        request.addBodyParams(params: ["key": "value"])
        XCTAssertEqual(String(data: request.httpBody!, encoding: .utf8), "key=value&")
    }

    func testURLEncodedTwoParams() {
        request.allHTTPHeaderFields = [ContentTypeHeader.key: ContentTypeHeader.urlEncoded.rawValue]

        request.addBodyParams(params: ["key1": "value1", "key2": "value2"])
        let paramsOrder1 = "key1=value1&key2=value2&"
        let paramsOrder2 = "key2=value2&key1=value1&"
        let body = String(data: request.httpBody!, encoding: .utf8)!
        guard body == paramsOrder1 || body == paramsOrder2 else {
            XCTFail("URLRequest doesn't have the given params in any order. Its body: \(body)")
            return
        }
    }

    func testURLEncodedTwoParamsWithAmpersand() {
        request.allHTTPHeaderFields = [ContentTypeHeader.key: ContentTypeHeader.urlEncoded.rawValue]

        request.addBodyParams(params: ["key&": "value", "key": "value&"])
        let paramsOrder1 = "key%26=value&key=value%26&"
        let paramsOrder2 = "key=value%26&key%26=value&"
        let body = String(data: request.httpBody!, encoding: .utf8)!
        guard body == paramsOrder1 || body == paramsOrder2 else {
            XCTFail("URLRequest doesn't have the given params in any order. Its body: \(body)")
            return
        }
    }

    func testJSONOneParam() {
        request.allHTTPHeaderFields = [ContentTypeHeader.key: ContentTypeHeader.json.rawValue]

        request.addBodyParams(params: ["key": "value"])
        XCTAssertEqual(String(data: request.httpBody!, encoding: .utf8), "{\"key\":\"value\"}")
    }

    func testJSONTwoParams() {
        request.allHTTPHeaderFields = [ContentTypeHeader.key: ContentTypeHeader.json.rawValue]

        request.addBodyParams(params: ["key1": "value1", "key2": "value2"])
        let paramsOrder1 = "{\"key1\":\"value1\",\"key2\":\"value2\"}"
        let paramsOrder2 = "{\"key2\":\"value2\",\"key1\":\"value1\"}"
        let body = String(data: request.httpBody!, encoding: .utf8)!
        guard body == paramsOrder1 || body == paramsOrder2 else {
            XCTFail("URLRequest doesn't have the given params in any order. Its body: \(body)")
            return
        }
    }
}
